<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nbcore
 */
$single_blog_sidebar = nbcore_get_options('nbcore_blog_sidebar');
$title_position = nbcore_get_options('nbcore_blog_single_title_position');
get_header();
    if('position-1' === $title_position) {
        echo '<div class="nb-page-title-wrap"><div class="container"><div class="nb-page-title">';
        nbcore_posted_on();
        the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
        nbcore_get_categories();
        echo '</div></div></div>';
    } ?>
	<div class="container">
		<div class="single-blog row <?php nbcore_blog_classes(); ?>">

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

				<?php
				while ( have_posts() ) : the_post(); ?>
					<div class="entry-content">
                        <?php if('position-2' === $title_position) {
                            nbcore_posted_on();
                            the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
                            nbcore_get_categories();
                        }?>
                        <?php
                        if(nbcore_get_options('nbcore_blog_single_show_thumb')):
                            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full' );
                        	if($thumb):
                            ?>
                            <div class="entry-image">
                                <a href="<?php the_permalink(); ?>">
                                    <?php
                                    printf('<img src="%1$s" title="%2$s" width="%3$s" height="%4$s" />',
                                        $thumb[0],
                                        esc_attr(get_the_title()),
                                        $thumb[1],
                                        $thumb[2]
                                    );
                                    ?>
                                </a>
                            </div>
                        <?php endif;
                        endif; ?>
						<div class="entry-text">
							<?php
							the_content( sprintf(
							/* translators: %s: Name of current post. */
								wp_kses( esc_html__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'core-wp' ), array( 'span' => array( 'class' => array() ) ) ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							) );

							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'core-wp' ),
								'after'  => '</div>',
							) );
							?>
						</div>
                        <div class="entry-footer">
						    <?php nbcore_get_tags(); ?>
                        </div>
						<?php
                        if('inside-content' === nbcore_get_options('share_buttons_position')) {
                            if(nbcore_get_options('nbcore_blog_single_show_social')) {
                                nbcore_share_social();
                            }
                        }
                        ?>
                        <?php if(nbcore_get_options('nbcore_blog_single_show_author')): ?>
						<div class="entry-author">
							<div class="author-image">
								<?php echo get_avatar(get_the_author_meta('ID'), 100); ?>
							</div>
							<div class="author-meta">
								<div class="author-name">
									<?php echo esc_html( get_the_author_meta( 'display_name' ) ); ?>
								</div>
								<?php $author_desc = get_the_author_meta('user_description');
								if($author_desc): ?>
								<div class="author-description">
									<?php echo esc_html($author_desc); ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
                        <?php endif; ?>
                        <?php if(nbcore_get_options('nbcore_blog_single_show_nav')): ?>
						<nav class="single-blog-nav" role="navigation">
							<?php
								previous_post_link( '<div class="prev">%link<span>' .  esc_html__( 'Previous post', 'core-wp' ) . '</span></div>', _x( '<span class="meta-nav"><i class="icon-left-open"></i></span>%title', 'Previous post', 'core-wp' ) );
								next_post_link(     '<div class="next">%link<span>' .  esc_html__( 'Next post', 'core-wp' ) . '</span></div>',     _x( '%title<span class="meta-nav"><i class="icon-right-open"></i></span>', 'Next post', 'core-wp' ) );
							?>
						</nav><!-- .single-nav -->
                        <?php endif; ?>
					</div>
                    <?php
                    if(nbcore_get_options('nbcore_blog_single_show_comments')) {
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        }
                    }
                    ?>
				<?php
				endwhile; // End of the loop.
				?>
	
				</main><!-- #main -->
			</div><!-- #primary -->
		<?php
        if('no-sidebar' !== $single_blog_sidebar) {
            get_sidebar();
        }
		?>
		</div>
	</div>
		

<?php
get_footer();
