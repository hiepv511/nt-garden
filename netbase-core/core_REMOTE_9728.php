<?php

define('NBT_VER', '1.0.0');

// define('NBT_CTP', 'netbase-core/templates/');

class NBT_Core
{
    /**
     * Class prefix for autoload
     *
     * @var string
     */
    protected static $prefix = 'NBT_';

    /**
     * Variable hold the page options
     *
     * @var array
     */
    protected static $page_options = array();

    public static function init()
    {
        spl_autoload_register(array(__CLASS__, 'autoload'));

        self::include_wp_function_plugins();

        NBT_Helper::include_template_tags();
        NBT_Extensions_Woocommerce::init();

        add_action('wp', array(__CLASS__, 'get_options'));
        add_action('customize_register', array('NBT_Customize', 'register'));

        add_action('after_setup_theme', array(__CLASS__, 'theme_setup'));
        add_action('widgets_init', array(__CLASS__, 'default_sidebars'));

        add_action('admin_enqueue_scripts', array(__CLASS__, 'admin_scripts_enqueue'));
        add_action('customize_controls_enqueue_scripts', array('NBT_Customize', 'customize_control_js'));
        add_action('customize_preview_init', array('NBT_Customize', 'customize_preview_js'));
        add_action('customize_controls_print_styles', array('NBT_Customize', 'customize_style'));

        //TODO make inline style below woocommerce.css.
        add_action('wp_enqueue_scripts', array(__CLASS__, 'core_scripts_enqueue'), 9998);
        add_action('wp_enqueue_scripts', array(__CLASS__, 'print_embed_style'), 9999);
        add_action('wp_enqueue_scripts', array(__CLASS__, 'google_fonts_url'));

        add_filter('body_class', array('NBT_Helper', 'nbcore_body_classes'));
        add_action('wp_head', array('NBT_Helper', 'nbcore_pingback_header'));

        add_filter( 'show_recent_comments_widget_style', '__return_false' );

        // Header Designer
//        add_action('nb_core_after_header', array(__CLASS__,'nbcore_render_header_preset'));
//
//        // Footer Designer
//        add_action('nb_core_before_footer', array(__CLASS__,'nbcore_render_footer_preset'));

        //function to parse google fonts
//        $this->ggfont_json();
    }


    // Render Header Template
    public static function nbcore_render_header_preset(){
        echo '<div class="container">';
        get_template_part('template-parts/header/header','wrapper');
        echo '</div>';
    }

    // Render Footer Template
    public static function nbcore_render_footer_preset(){
        echo '<div class="container">';
        get_template_part('template-parts/footer/footer','wrapper');
        echo '</div>';
    }


    public static function autoload($class_name)
    {
        // Verify class prefix.
        if (0 !== strpos($class_name, self::$prefix)) {
            return false;
        }

//        echo '<pre>';
//        var_dump($class_name);
//        echo '</pre>';

        // Generate file path from class name.
        $base = get_template_directory() . '/netbase-core/';
        $path = strtolower(str_replace('_', '/', substr($class_name, strlen(self::$prefix))));

        // Check if class file exists.
        $standard = $path . '.php';
        $alternative = $path . '/' . current(array_slice(explode('/', str_replace('\\', '/', $path)), -1)) . '.php';

        while (true) {
            // Check if file exists in standard path.
            if (@is_file($base . $standard)) {
                $exists = $standard;

                break;
            }

            // Check if file exists in alternative path.
            if (@is_file($base . $alternative)) {
                $exists = $alternative;

                break;
            }

            // If there is no more alternative file, quit the loop.
            if (false === strrpos($standard, '/') || 0 === strrpos($standard, '/')) {
                break;
            }

            // Generate more alternative files.
            $standard = preg_replace('#/([^/]+)$#', '-\\1', $standard);
            $alternative = implode('/', array_slice(explode('/', str_replace('\\', '/', $standard)), 0, -1)) . '/' . substr(current(array_slice(explode('/', str_replace('\\', '/', $standard)), -1)), 0, -4) . '/' . current(array_slice(explode('/', str_replace('\\', '/', $standard)), -1));
        }

        // Include class declaration file if exists.
        if (isset($exists)) {
            return include_once $base . $exists;
        }

        return false;
    }

    public static function include_wp_function_plugins()
    {
        if ( ! function_exists( 'is_plugin_active' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }
    }

    public static function theme_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on core-wp, use a find and replace
         * to change 'core-wp' to the name of your theme in all the template files.
         */
        load_theme_textdomain('core-wp', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // A theme must have at least one navbar, right?
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'core-wp'),
            'header-sub' => esc_html__('Header sub menu', 'core-wp'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         * See https://developer.wordpress.org/themes/functionality/post-formats/
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'audio',
            'video',
            'quote',
            'link',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('core_wp_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        add_image_size('nbcore-masonry', 450, 450, true);
    }

    /**
     * Theme default sidebar.
     *
     * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
     */
    public static function default_sidebars()
    {
        register_sidebar(array(
            'name' => esc_html__('Default Sidebar', 'core-wp'),
            'id' => 'default-sidebar',
            'description' => esc_html__('Add widgets here.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Shop Sidebar', 'core-wp'),
            'id' => 'shop-sidebar',
            'description' => esc_html__('Add widgets for category page.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Product Sidebar', 'core-wp'),
            'id' => 'product-sidebar',
            'description' => esc_html__('Add widgets for product details page', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top #1', 'core-wp'),
            'id' => 'footer-top-1',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top #2', 'core-wp'),
            'id' => 'footer-top-2',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top #3', 'core-wp'),
            'id' => 'footer-top-3',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top #3', 'core-wp'),
            'id' => 'footer-top-3',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top #4', 'core-wp'),
            'id' => 'footer-top-4',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom #1', 'core-wp'),
            'id' => 'footer-bot-1',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom #2', 'core-wp'),
            'id' => 'footer-bot-2',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom #3', 'core-wp'),
            'id' => 'footer-bot-3',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Bottom #4', 'core-wp'),
            'id' => 'footer-bot-4',
            'description' => esc_html__('For best display, please assign only one widget in this section.', 'core-wp'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
    }

    // Todo change to minified version and load conditional. Example: isotope is now always load
    public static function core_scripts_enqueue()
    {
        //TODO Remember this
        wp_dequeue_script('wc-cart');
        wp_enqueue_style('fontello', get_template_directory_uri() . '/assets/vendor/fontello/fontello.css', array(), NBT_VER);

        wp_enqueue_style('nbcore_front_style', get_template_directory_uri() . '/assets/netbase/css/main.css', array(), NBT_VER);

        wp_enqueue_style('nbcore_woo_style', get_template_directory_uri() . '/assets/netbase/css/woocommerce.css', array(), NBT_VER);

        // wp_enqueue_script('masonry');
        wp_enqueue_script('isotope', get_template_directory_uri() . '/assets/vendor/isotope/isotope.pkdg.min.js', array('jquery'), '3.0.3', true);

        if( function_exists('is_product') && is_product() || function_exists('is_cart') && is_cart() ) {
            wp_enqueue_style('flickity', get_template_directory_uri() . '/assets/vendor/flickity/flickity.min.css', array(), '2.0.5');
            wp_enqueue_script('flickity', get_template_directory_uri() . '/assets/vendor/flickity/flickity.pkgd.min.js', array('jquery'), '2.0.5', true);
            wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/assets/vendor/magnific-popup/magnific-popup.css', array(), '2.0.5');
            wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/vendor/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), '2.0.5', true);
        }

        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

        if(function_exists('is_product') && is_product() && 'accordion-tabs' == nbcore_get_options('nbcore_info_style')) {
            wp_enqueue_script( 'jquery-ui-accordion' );
        }

        if(nbcore_get_options('nbcore_header_fixed')) {
            wp_enqueue_script('waypoints',  get_template_directory_uri() . '/assets/vendor/waypoints/jquery.waypoints.min.js', array('jquery'), '4.0.1', true);
        }

        if(nbcore_get_options('nbcore_blog_sticky_sidebar')) {
            wp_enqueue_script('sticky-kit',  get_template_directory_uri() . '/assets/vendor/sticky-kit/jquery.sticky-kit.min.js', array('jquery'), '1.1.2', true);
        }

        wp_enqueue_script('nbcore_front_script', get_template_directory_uri() . '/assets/netbase/js/main.js', array('jquery'), NBT_VER, true);
        $localize_array = array(
            'upsells_columns' => nbcore_get_options('nbcore_pd_upsells_columns'),
            'related_columns' => nbcore_get_options('nbcore_pd_related_columns'),
            'cross_sells_columns' => nbcore_get_options('nbcore_cross_sells_per_row'),
        );
        wp_localize_script('nbcore_front_script', 'nb', $localize_array);
    }

    public static function admin_scripts_enqueue()
    {
        wp_enqueue_style('fontello', get_template_directory_uri() . '/assets/vendor/fontello/fontello.css', array(), NBT_VER);
    }

    public static function get_options()
    {
        static $prepared;

        if (!isset($prepared)) {
            // Get all customizer options
            $theme_mods = NBT_Customize::get_options();

            if ($theme_mods && is_array($theme_mods)) {
                self::$page_options = array_merge(self::$page_options, $theme_mods);
            }

            if (function_exists('get_fields')) {
                $page_id = get_the_ID();

                if ($page_id) {
                    // Todo replace acf ?
                    $post_meta = get_fields();
                    if ($post_meta) {
                        foreach ($post_meta as $k => $v) {
                            self::$page_options[$k] = maybe_unserialize($v);
                        }
                    }
                }

                if (isset(self::$page_options)) {
                    // Merge with theme options with higher priority.
                    self::$page_options = array_merge(
                        $theme_mods,
                        self::$page_options
                    );
                }
            }

            $prepared = true;
        }

        if (!doing_action('wp')) {
            // Prepare options to return.
            if (isset($page_id) && isset(self::$page_options[$page_id])) {
                $options = self::$page_options[$page_id];
            }

            return self::$page_options;
        }

    }

    //TODO optimize this(grouping and bring to css if can)
    //TODO early esc_
    public static function get_embed_style()
    {
//         $theme_options = self::get_options();


        $bg_color = nbcore_get_options('nbcore_background_color');
        $inner_bg = nbcore_get_options('nbcore_inner_background');
        if(function_exists('get_field')) {
            if (get_field('nbcore_background_header_color') == null) {
                $header_bgcolor = nbcore_get_options('header_bgcolor');
            }else{
                $header_bgcolor = get_field('nbcore_background_header_color');
            }
        }

        $top_padding = nbcore_get_options('nbcore_top_section_padding');
        $top_bg = nbcore_get_options('nbcore_header_top_bg');
        $top_color = nbcore_get_options('nbcore_header_top_color');
        $middle_padding = nbcore_get_options('nbcore_middle_section_padding');
        $middle_bg = nbcore_get_options('nbcore_header_middle_bg');
        $middle_color = nbcore_get_options('nbcore_header_middle_color');
        $bot_padding = nbcore_get_options('nbcore_bot_section_padding');
        $bot_bg = nbcore_get_options('nbcore_header_bot_bg');
        $bot_color = nbcore_get_options('nbcore_header_bot_color');

//        $header_bgcolor = nbcore_get_options('header_bgcolor');
        $logo_area_width = nbcore_get_options('nbcore_logo_width');
        $blog_width = nbcore_get_options('nbcore_blog_width');
        $primary_color = nbcore_get_options('nbcore_primary_color');
        $divider_color = nbcore_get_options('nbcore_divider_color');

        $heading_styles_array = explode(",", nbcore_get_options('heading_font_family'));
        $heading_family = $heading_styles_array[0];
        $heading_weight = end($heading_styles_array);
        $heading_color = nbcore_get_options('nbcore_heading_color');

        $heading_base_size = nbcore_get_options('heading_base_size');

        $body_styles_array = explode(",", nbcore_get_options('body_font_family'));
        $body_family = $body_styles_array[0];
        $body_weight = end($body_styles_array);
        $body_color = nbcore_get_options('nbcore_body_color');
        $body_size = nbcore_get_options('body_font_size');

//        $navigation_styles_array = explode(",", nbcore_get_options('navigation_font_family'));
//        $navigation_family = $navigation_styles_array[0];
//        $navigation_weight = end($navigation_styles_array);
//        $navigation_color = nbcore_get_options('nbcore_nav_color');
//        $navigation_size = nbcore_get_options('navigation_font_size');

        $link_color = nbcore_get_options('nbcore_link_color');
        $link_hover_color = nbcore_get_options('nbcore_link_hover_color');

        $blog_sidebar = nbcore_get_options('nbcore_blog_sidebar');
        $page_title_padding = nbcore_get_options('nbcore_page_title_padding');
        $page_title_img = wp_get_attachment_url(absint(nbcore_get_options('nbcore_page_title_image')));

        $page_title_color = nbcore_get_options('nbcore_page_title_color');

        $wc_content_width = nbcore_get_options('nbcore_wc_content_width');
        $shop_sidebar = nbcore_get_options('nbcore_shop_sidebar');
        $loop_columns = nbcore_get_options('nbcore_loop_columns');
        $pd_details_sidebar = nbcore_get_options('nbcore_pd_details_sidebar');
        $pd_details_width = nbcore_get_options('nbcore_pd_details_width');
        $pd_images_width = nbcore_get_options('nbcore_pd_images_width');

        $pb_bg = nbcore_get_options('nbcore_pb_background');
        $pb_bg_hover = nbcore_get_options('nbcore_pb_background_hover');
        $pb_text = nbcore_get_options('nbcore_pb_text');
        $pb_text_hover = nbcore_get_options('nbcore_pb_text_hover');
        $pb_border = nbcore_get_options('nbcore_pb_border');
        $pb_border_hover = nbcore_get_options('nbcore_pb_border_hover');
        $sb_bg = nbcore_get_options('nbcore_sb_background');
        $sb_bg_hover = nbcore_get_options('nbcore_sb_background_hover');
        $sb_text = nbcore_get_options('nbcore_sb_text');
        $sb_text_hover = nbcore_get_options('nbcore_sb_text_hover');
        $sb_border = nbcore_get_options('nbcore_sb_border');
        $sb_border_hover = nbcore_get_options('nbcore_sb_border_hover');
        $button_padding = nbcore_get_options('nbcore_button_padding');
        $button_border_radius = nbcore_get_options('nbcore_button_border_radius');
        $button_border_width = nbcore_get_options('nbcore_button_border_width');

        $footer_top_heading = nbcore_get_options('nbcore_footer_top_heading');
        $footer_top_color = nbcore_get_options('nbcore_footer_top_color');
        $footer_top_bg = nbcore_get_options('nbcore_footer_top_bg');
        $footer_bot_heading = nbcore_get_options('nbcore_footer_bot_heading');
        $footer_bot_color = nbcore_get_options('nbcore_footer_bot_color');
        $footer_bot_bg = nbcore_get_options('nbcore_footer_bot_bg');
        $footer_abs_bg = nbcore_get_options('nbcore_footer_abs_bg');
        $footer_abs_color = nbcore_get_options('nbcore_footer_abs_color');

        $blog_title_size = nbcore_get_options('nbcore_blog_single_title_size');
        $page_title_size = nbcore_get_options('nbcore_page_title_size');

        $footer_abs_padding = nbcore_get_options('nbcore_footer_abs_padding');

        $css = "
            #site-wrapper {
                background: " . esc_attr($bg_color) . ";
            }
            .nb-page-title-wrap,
            .single-blog .entry-author,
            .products .list-type-wrap,
            .shop-main.accordion-tabs .accordion-title-wrap,
            .woocommerce .woocommerce-message,
            .woocommerce .woocommerce-info,
            .woocommerce .woocommerce-error,
            .woocommerce-page .woocommerce-message,
            .woocommerce-page .woocommerce-info,
            .woocommerce-page .woocommerce-error,
            .cart-layout-2 .cart-totals-wrap,
            .blog.style-2 .post .entry-content,
            .nb-comment-form textarea,
            .comments-area
            {
                background-color: " . esc_attr($inner_bg) . ";
            }
            .products.list-type .product .list-type-wrap .product-image:before {
                border-right-color: " . esc_attr($inner_bg) . ";
            }
            .main-logo {
                width: " . esc_attr($logo_area_width) . "px;
            }
            .site-header {
                background: " . esc_attr($header_bgcolor) . ";
            }
            a,
            .widget ul li a:hover,
            .footer-top-section a:hover,
            .footer-top-section .widget ul li a:hover,
            .footer-bot-section a:hover,
            .footer-bot-section .widget ul li a:hover{
                color: " . esc_attr($link_color) . ";
            }
            a:hover, a:focus, a:active{
                color: " . esc_attr($link_hover_color) . ";
            }
            body {
                font-family: " . esc_attr($body_family) . "; 
                font-weight: " . esc_attr($body_weight) . ";                
                font-size: " . esc_attr($body_size) . "px;
        ";
        if (in_array("italic", $body_styles_array)) {
            $css .= "
                font-style: italic;
            ";
        }
        if (in_array("underline", $body_styles_array)) {
            $css .= "
                text-decoration: underline;
            ";
        }
        if (in_array("uppercase", $body_styles_array)) {
            $css .= "
                text-transform: uppercase;
            ";
        }
        $css .= "
            }
            .button, .nb-primary-button {
                color: " . esc_attr($pb_text) . " !important;
                background-color: " . esc_attr($pb_bg) . ";
                border-color: " . esc_attr($pb_border) . ";
            }
            .button:hover, .nb-primary-button:hover, .button:focus, .nb-primary-button:focus {
                color: " . esc_attr($pb_text_hover) . ";
                background-color: " . esc_attr($pb_bg_hover) . ";
                border-color: " . esc_attr($pb_border_hover) . ";
            }
            .nb-secondary-button {
                color: " . esc_attr($sb_text) . ";
                background-color: " . esc_attr($sb_bg) . ";
                border-color: " . esc_attr($sb_border) . ";
            }
            .nb-secondary-button:hover, .nb-secondary-button:focus {
                color: " . esc_attr($sb_text_hover) . ";
                background-color: " . esc_attr($sb_bg_hover) . ";
                border-color: " . esc_attr($sb_border_hover) . ";
            }
            .list-type .add_to_cart_button, .nb-primary-button, .nb-secondary-button, .single_add_to_cart_button {
                padding-left: " .esc_attr($button_padding) . "px;
                padding-right: " .esc_attr($button_padding) . "px;
                border-width: " . esc_attr($button_border_width) . "px;
            ";
        if($button_border_radius) {
            $css .= "
                border-radius: " . esc_attr($button_border_radius) . "px;
            ";
        } else {
            $css .= "
                border-radius: 0px;
            ";
        }
        $css .= "
            }
            body,
            .widget ul li a,
            .woocommerce-breadcrumb a,
            .nb-social-icons > a,
            .wc-tabs > li:not(.active) a,
            .shop-main.accordion-tabs .accordion-title-wrap:not(.ui-state-active) a,
            .nb-account-dropdown a,
            .header-account-wrap .not-logged-in,
            .mid-inline .nb-account-dropdown a, 
            .mid-inline .mini-cart-section span, 
            .mid-inline .mini-cart-section a, 
            .mid-inline .mini-cart-section strong{
                color: " . esc_attr($body_color) . ";
            }
            h1 {
                font-size: " . esc_attr(intval($heading_base_size * 3.157)) . "px;
            }
            h2 {
                font-size: " . esc_attr(intval($heading_base_size * 2.369)) . "px;
            }
            h3 {
                font-size: " . esc_attr(intval($heading_base_size * 1.777)) . "px;
            }
            h4 {
                font-size: " . esc_attr(intval($heading_base_size * 1.333)) . "px;
            }
            h5 {
                font-size: " . esc_attr(intval($heading_base_size * 1)) . "px;
            }
            h6 {
                font-size: " . esc_attr(intval($heading_base_size * 0.75)) . "px;
            }
            h1, h2, h3, h4, h5, h6,
            h1 > a, h2 > a, h3 > a, h4 > a, h5 > a, h6 > a,
            .entry-title > a,
            .woocommerce-Reviews .comment-reply-title {
                font-family: " . esc_attr($heading_family) . "; 
                font-weight: " . esc_attr($heading_weight) . ";
                color: " . esc_attr($heading_color) . ";
        ";
        if (in_array("italic", $heading_styles_array)) {
            $css .= "
                font-style: italic;
            ";
        }
        if (in_array("underline", $heading_styles_array)) {
            $css .= "
                text-decoration: underline;
            ";
        }
        if (in_array("uppercase", $heading_styles_array)) {
            $css .= "
                text-transform: uppercase;
            ";
        }
        //TODO after make inline below woocommerce.css remove these !important
        //TODO postMessage font-size .header-top-bar a
        $css .= "
            }
            .site-header .top-section-wrap {
                padding: " . esc_attr($top_padding) . "px 0;
                background-color: " . esc_attr($top_bg) . ";
            }
            .top-section-wrap .nb-header-sub-menu a {
                color: " . esc_attr($top_color) . ";
            }
            .top-section-wrap .nb-header-sub-menu .sub-menu {
                background-color: " . esc_attr($top_bg) . ";
            }
            .site-header .middle-section-wrap {
                padding: " . esc_attr($middle_padding) . "px 0;
                background-color: " . esc_attr($middle_bg) . ";
            }
            .site-header:not(.mid-stack) .bot-section-wrap {
                padding: " . esc_attr($bot_padding) . "px 0;                
            }
            .site-header.mid-stack .nb-navbar > .menu-item > a {
                padding: " . esc_attr($bot_padding) . "px 20px;                
            }
            .site-header .bot-section-wrap {
                background-color: " . esc_attr($bot_bg) . ";           
            }
            .bot-section-wrap a, .bot-section-wrap span, .bot-section-wrap i, .bot-section-wrap div{
                color: " . esc_attr($bot_color) . ";
            }
            .middle-section-wrap a, .middle-section-wrap span, .middle-section-wrap i, .middle-section-wrap div{
                color: " . esc_attr($middle_color) . ";
            }
            .top-section-wrap a, .top-section-wrap span, .top-section-wrap i, .top-section-wrap div{
                color: " . esc_attr($top_color) . ";
            }
            .nb-navbar .menu-item-has-children > a span:after,
            .icon-header-section .nb-cart-section,
            .nb-navbar .menu-item a,
            .nb-navbar .sub-menu > .menu-item:not(:last-child),
            .nb-header-sub-menu .sub-menu > .menu-item:not(:last-child),
            .widget .widget-title,
            .blog .classic .post .entry-footer,
            .single-post .single-blog .entry-footer,
            .nb-social-icons > a,
            .single-blog .single-blog-nav,
            .shop-main:not(.wide) .single-product-wrap .entry-summary .cart,
            .shop-main.accordion-tabs .accordion-item .accordion-title-wrap,
            .shop-main.horizontal-tabs .wc-tabs-wrapper,
            .shop_table thead th,
            .shop_table th,
            .shop_table td,
            .mini-cart-wrap .total,
            .icon-header-wrap .nb-account-dropdown ul li:not(:last-of-type) a,
            .widget tbody th, .widget tbody td,
            .widget ul > li:not(:last-of-type),
            .blog .post .entry-image .entry-cat,
            .comment-list .comment,
            .nb-comment-form textarea,
            .paging-navigation.pagination-style-1 .page-numbers.current,
            .woocommerce-pagination.pagination-style-1 .page-numbers.current{
                border-color: " . esc_attr($divider_color) . ";
            }  
            .product .product-image .onsale,
            .wc-tabs > li.active,
            .product .onsale.sale-style-2 .percent,
            .wc-tabs-wrapper .woocommerce-Reviews #review_form_wrapper .comment-respond,
            .site-header.mid-stack .main-navigation .nb-navbar > .menu-item:hover,
            .shop-main.accordion-tabs .accordion-item .accordion-title-wrap.ui-accordion-header-active,
            .widget .tagcloud a,
            .footer-top-section .widget .tagcloud a,
            .footer-bot-section .widget .tagcloud a
            {
                border-color: " . esc_attr($primary_color) . ";
            }
            .widget .widget-title:before,
            .paging-navigation.pagination-style-2 .current,
            .product .onsale.sale-style-1,
            .woocommerce-pagination.pagination-style-2 span.current,
            .shop-main.right-dots .flickity-page-dots .dot,
            .wc-tabs-wrapper .form-submit input,
            .nb-input-group .search-button button,
            .widget .tagcloud a:hover,
            .nb-back-to-top-wrap a:hover{
                background-color: " . esc_attr($primary_color) . ";
            }
            .product .star-rating:before,
            .product .star-rating span,
            .single-product-wrap .price ins,
            .single-product-wrap .price > span.amount,
            .wc-tabs > li.active a,
            .wc-tabs > li.active a:hover,
            .wc-tabs > li.active a:focus,
            .wc-tabs .ui-accordion-header-active a,
            .wc-tabs .ui-accordion-header-active a:focus,
            .wc-tabs .ui-accordion-header-active a:hover,
            .shop-main.accordion-tabs .ui-accordion-header-active:after,
            .shop_table .cart_item td .amount,
            .cart_totals .order-total .amount,
            .shop_table.woocommerce-checkout-review-order-table .order-total .amount,
            .woocommerce-order .woocommerce-thankyou-order-received,
            .woocommerce-order .woocommerce-table--order-details .amount,
            .paging-navigation.pagination-style-1 .current,
            .woocommerce-pagination.pagination-style-1 .page-numbers.current{
                color: " . esc_attr($primary_color) . ";                
            }
            .nb-page-title-wrap {
                padding-top: " . esc_attr($page_title_padding) . "px;
                padding-bottom: " . esc_attr($page_title_padding) . "px;
            ";
        if ($page_title_img) {
            $css .= "
                background-image: url(" . esc_url($page_title_img) . ");
                background-size: cover;
                background-repeat: no-repeat;
                ";
        }
        $css .= "
            }
            .nb-page-title-wrap a, .nb-page-title-wrap h1, .nb-page-title-wrap nav {
                color: " . esc_attr($page_title_color) . ";
            }            
            .nb-page-title-wrap h1 {
                font-size: " . esc_attr($page_title_size) . "px;
            }
            .woocommerce-page.wc-no-sidebar #primary {
                width: 100%;
            }
            .shop-main .products.grid-type .product:nth-child(" . esc_attr($loop_columns) . "n + 1) {
                clear: both;
            }                       
            .shop-main:not(.wide) .single-product-wrap .product-image {
                flex: 0 0 " . esc_attr($pd_images_width) . "%;
                -ms-flex: 0 0 " . esc_attr($pd_images_width) . "%;
                max-width: " . esc_attr($pd_images_width) . "%;
            }
            .shop-main:not(.wide) .single-product-wrap .entry-summary {
                flex: 0 0 calc(100% - " . esc_attr($pd_images_width) . "%);
                -ms-flex: 0 0 calc(100% - " . esc_attr($pd_images_width) . "%);
                max-width: calc(100% - " . esc_attr($pd_images_width) . "%);
            }
        ";
        if ('no-sidebar' !== $blog_sidebar) {
            $css .= "
            .site-content .blog #primary, .site-content .single-blog #primary {
                width: " . esc_attr($blog_width) . "%;
            }            
            .site-content .blog #secondary, .site-content .single-blog #secondary {
                width: calc(100% - " . esc_attr($blog_width) . "%);
            }
            ";
        }
        // TODO check this for tag ... ?
        if ('no-sidebar' !== $shop_sidebar) {
            $css .= "
            .archive.woocommerce.wc-has-sidebar .shop-main{
                width: " . esc_attr($wc_content_width) . "%;
            }
            .archive.woocommerce.wc-has-sidebar #secondary {
                width: calc(100% - " . esc_attr($wc_content_width) . "%);
            }
            ";
        } else {
            $css .= "
            .site-content .shop-main {
                width: 100%;
            }
            ";
        }
        // TODO check responsive
        if ('left-sidebar' == $blog_sidebar) {
            $css .= "
            .single-blog #primary , .blog #primary {
                order: 2;
            }
            ";
        }
        if ('left-sidebar' == $shop_sidebar) {
            $css .= "
            .archive.woocommerce .shop-main {
                order: 2;
            }
            ";
        }
        if('no-sidebar' !== $pd_details_sidebar) {
            $css .= "
            .single-product.wc-has-sidebar .shop-main {
                width: " . esc_attr($pd_details_width) . "%;
            }
            .single-product #secondary {
                width: calc(100% - " . esc_attr($pd_details_width) . "%);
            }
            ";
        } else {
            $css .= "
            .single-product.wc-has-sidebar .shop-main {
                width: 100%;
            }
            ";
        }
        if ('left-sidebar' == $pd_details_sidebar) {
            $css .= "
            .single-product .shop-main {
                order: 2;
            }
            ";
        }

        $css .= "
            .footer-top-section {                
                background-color: " . esc_attr($footer_top_bg) . ";
            }
            .footer-top-section h1,
            .footer-top-section h2,
            .footer-top-section h3,
            .footer-top-section h4,
            .footer-top-section h5,
            .footer-top-section h6,
            .footer-top-section .widget-title a{
                color: " . esc_attr($footer_top_heading) . ";
            }           
            .footer-top-section,
            .footer-top-section a,
            .footer-top-section .widget ul li a{
                color: " . esc_attr($footer_top_color) . ";
            }
            .footer-top-section .widget .tagcloud a{
                border-color: " . esc_attr($footer_top_color) . ";
            }            
            .footer-bot-section {
                background-color: " . esc_attr($footer_bot_bg) . ";
            }
            .footer-bot-section h1,
            .footer-bot-section h2,
            .footer-bot-section h3,
            .footer-bot-section h4,
            .footer-bot-section h5,
            .footer-bot-section h6,
            .footer-bot-section .widget-title a{
                color: " . esc_attr($footer_bot_heading) . ";
            }
            .footer-bot-section,
            .footer-bot-section a,
            .footer-bot-section .widget ul li a{
                color: " . esc_attr($footer_bot_color) . ";
            }
            .footer-bot-section .widget .tagcloud a{
                border-color: " . esc_attr($footer_bot_color) . ";
            }
            .footer-abs-section {
                color: " . esc_attr($footer_abs_color) . ";
                background-color: " . esc_attr($footer_abs_bg) . ";
                padding-top: " . esc_attr($footer_abs_padding) . "px;
                padding-bottom: " . esc_attr($footer_abs_padding) . "px;
            }
            .footer-abs-section a, .footer-abs-section p {
                color: " . esc_attr($footer_abs_color) . ";
            }
            .single-blog .nb-page-title .entry-title,
            .single-blog .entry-title{
                font-size: " . esc_attr($blog_title_size) . "px;
            }
        ";

        return $css;
    }

    public static function print_embed_style()
    {
        $style = self::get_embed_style();

        $style = preg_replace('#/\*.*?\*/#s', '', $style);
        $style = preg_replace('/\s*([{}|:;,])\s+/', '$1', $style);
        $style = preg_replace('/\s\s+(.*)/', '$1', $style);

        //TODO Fix this hack? naming z_ to make this css final
        wp_register_style( 'z_nbcore_front_style', false );
        wp_enqueue_style( 'z_nbcore_front_style' );
        wp_add_inline_style('z_nbcore_front_style', $style);
    }

    public static function google_fonts_url()
    {
        $font_subset = 'latin,latin-ext';
        $font_families = array();
//        $font_weight = array();
//        
        $font_families['Noto Sans'] = array(
            '400',
            '700',
        );

        $font_families['Playfair Display'] = array(
            '400',
            '700',
            '900'
        );

        if (nbcore_get_options('body_font_family')) {
            $body_font = explode(",", nbcore_get_options('body_font_family'));
            $font_name = $body_font[0];
            $font_weight = array(end($body_font));
            $font_families[$font_name] = isset($font_families[$font_name]) ? array_unique(array_merge($font_families[$font_name], $font_weight)) : $font_weight;
        }

        if (nbcore_get_options('heading_font_family')) {
            $heading_font = explode(",", nbcore_get_options('heading_font_family'));
            $font_name = $heading_font[0];
            $font_weight = array(end($heading_font));
            $font_families[$font_name] = isset($font_families[$font_name]) ? array_unique(array_merge($font_families[$font_name], $font_weight)) : $font_weight;
        }

        if (nbcore_get_options('navigation_font_family')) {
            $nav_font = explode(",", nbcore_get_options('navigation_font_family'));
            $font_name = $nav_font[0];
            $font_weight = array(end($nav_font));
            $font_families[$font_name] = isset($font_families[$font_name]) ? array_unique(array_merge($font_families[$font_name], $font_weight)) : $font_weight;
        }

        $font_parse = array();
        foreach ($font_families as $font_name => $font_weight) {
            $font_parse[] = $font_name . ':' . implode(',', $font_weight);
        }

        if (nbcore_get_options('subset_cyrillic')) {
            $font_subset .= ',cyrillic,cyrillic-ext';
        }
        if (nbcore_get_options('subset_greek')) {
            $font_subset .= ',greek,greek-ext';
        }
        if (nbcore_get_options('subset_vietnamese')) {
            $font_subset .= ',vietnamese';
        }

        $query_args = array(
            'family' => urldecode(implode('|', $font_parse)),
            'subset' => urldecode($font_subset),
        );

        $font_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');

        $enqueue = esc_url_raw($font_url);

        wp_enqueue_style('nb-google-fonts', $enqueue);
    }

    //function to parse google fonts
//    public static function ggfont_json()
//    {
//        $str = file_get_contents('https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyCYxiXXri4OXAxMg1WfvHNSo5tBlJmKqWI');
//
//        $json = json_decode($str, true);
//
//        $family_array = array();
//
////        for($i = 0; $i < count($json['items']); $i++) {
//////            $name = str_replace(" ", "_", $json['items'][$i]['family']);
////            $name = $json['items'][$i]['family'];
//////            $slug = str_replace(" ", "+", $json['items'][$i]['family']);
//////            $family_array[$name]['family'] = "'" . $slug . "',";
////            $family_array[$name] = "'" . implode(',', $json['items'][$i]['variants']) . "',";
////        }
//
//        for($i = 0; $i < count($json['items']); $i++) {
////            $name = str_replace(" ", "_", $json['items'][$i]['family']);
//            $name = $json['items'][$i]['family'];
//            $family_array[$name] = "'" . implode(',', $json['items'][$i]['variants']) . "',";
//        }
//
//        echo '<pre>';
//        print_r($family_array);
//        echo '</pre>';;
//    }
}

NBT_Core::init();