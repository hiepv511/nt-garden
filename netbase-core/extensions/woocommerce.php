<?php
/**
 * Extend and customize Woocommerce
 */
class NBT_Extensions_Woocommerce {
	protected static $init = false;

	public static function init()
	{
		//TODO Fix this Cheat
//		$product_list = nbcore_get_options('nbcore_product_list');

//		if(self::$init) {
//			return;
//		}

//		if('grid-type' == $product_list) {
//			if(nbcore_get_options('nbcore_grid_product_description')) {
//				add_action('woocommerce_after_shop_loop_item_title', array(__CLASS__, 'product_description'), 15);
//			}
//		}

//		remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
		remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
		remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
		remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
		remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
		remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');

		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		add_filter( 'woocommerce_before_main_content', 'nbcore_page_title', 5 );
		add_filter( 'loop_shop_columns', array(__CLASS__, 'loop_columns') );
		add_filter( 'loop_shop_per_page', array(__CLASS__, 'products_per_page'), 20 );
		add_filter( 'woocommerce_pagination_args', array(__CLASS__, 'woocommerce_pagination') );
		add_filter('woocommerce_product_description_heading', '__return_empty_string');
		add_filter('woocommerce_product_additional_information_heading', '__return_empty_string');
		add_filter('woocommerce_review_gravatar_size', array(__CLASS__, 'wc_review_avatar_size'));
		add_filter('woocommerce_cross_sells_total', array(__CLASS__, 'cross_sells_limit'));
		add_filter('woocommerce_upsells_total', array(__CLASS__, 'upsells_limit'));
//		add_filter('woocommerce_add_to_cart_fragments', array(__CLASS__, 'header_cart_icon_fragment'));
//		add_filter('woocommerce_add_to_cart_fragments', array(__CLASS__, 'mini_cart_fragments'));
//		add_filter('woocommerce_product_review_comment_form_args', array(__CLASS__, 'wc_review_form_heading'));

		add_action('woocommerce_after_shop_loop_item', array(__CLASS__, 'product_action_div_open'), 6);
		add_action('woocommerce_after_shop_loop_item', array(__CLASS__, 'product_action_div_close'), 11);
		add_action('woocommerce_shop_loop_item_title', array(__CLASS__, 'product_title'), 10);
		add_action('woocommerce_before_main_content', array(__CLASS__, 'shop_banner'), 15);
//		add_action('woocommerce_single_product_summary', array(__CLASS__, 'product_details_title'), 5);
		add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15);
		add_action('woocommerce_single_product_summary', array(__CLASS__, 'wide_meta_left_div_open'), 9);
		add_action('woocommerce_single_product_summary', array(__CLASS__, 'wide_meta_left_div_close'), 24);
		add_action('woocommerce_single_product_summary', array(__CLASS__, 'wide_meta_right_div_open'), 26);
		add_action('woocommerce_single_product_summary', array(__CLASS__, 'wide_meta_right_div_close'), 55);
		add_action('woocommerce_share', array(__CLASS__, 'wc_share_social'));
		add_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 15);

	}

	public static function loop_columns()
	{
		return nbcore_get_options('nbcore_loop_columns');
	}

	public static function product_action_div_open()
	{
		echo '<div class="product-action">';
	}

	public static function product_action_div_close()
	{
		echo '</div>';
	}

	public static function product_title()
	{
		echo '<h4 class="product-title"><a href="' . esc_url(get_the_permalink()) . '">' . esc_html(get_the_title()) . '</a></h4>';
	}

	public static function products_per_page($cols)
	{
		return nbcore_get_options('nbcore_products_per_page');
	}

	public static function woocommerce_pagination()
	{
		return array(
			'prev_text' => '<i class="icon-left-open"></i>',
			'next_text' => '<i class="icon-right-open"></i>',
			'end_size' => 1,
			'mid_size' => 1,
		);
	}

	public static function product_description()
	{
		echo '<p class="product-description">' . esc_html(get_the_excerpt()) . '</p>';
	}

	public static function product_category()
	{
		global $post;
		$terms = get_the_terms( $post->ID, 'product_cat' );
		foreach ($terms as $term) {
			echo '<a class="product-category-link" href="' . esc_url(get_term_link($term->term_id)) . '">' . esc_html($term->name) . '</a>';
		}
	}

	public static function shop_banner()
	{
		if(function_exists( 'is_shop' ) && is_shop()) {
			$shop_banner_url = nbcore_get_options('nbcore_shop_banner');
			if ($shop_banner_url) {
				echo '<div class="shop-banner"><img src="' . esc_url(wp_get_attachment_url(absint($shop_banner_url))) . '" /></div>';
			}
		}
	}

//	public static function product_details_title()
//	{
//		$page_title = nbcore_get_options('show_title_section');
//		
//			
//		if( ! $page_title ) {
//			woocommerce_template_single_title();
//		}
//	}

	public static function wc_review_avatar_size()
	{
		return '80';
	}

	public static function wide_meta_left_div_open()
	{
		if('wide' === nbcore_get_options('nbcore_pd_meta_layout')) {
			echo '<div class="pd-meta-left">';
		}
	}

	public static function wide_meta_left_div_close()
	{
		if('wide' === nbcore_get_options('nbcore_pd_meta_layout')) {
			echo '</div>';
		}
	}

	public static function wide_meta_right_div_open()
	{
		if('wide' === nbcore_get_options('nbcore_pd_meta_layout')) {
			echo '<div class="pd-meta-right">';
		}
	}

	public static function wide_meta_right_div_close()
	{
		if('wide' === nbcore_get_options('nbcore_pd_meta_layout')) {
			echo '</div>';
		}
	}

	public static function wc_share_social()
	{
		if(nbcore_get_options('nbcore_pd_show_social')) {
			nbcore_share_social();
		}
	}

	public static function cross_sells_limit()
	{
		$cross_sells_limit = nbcore_get_options('nbcore_cross_sells_limit');
		return $cross_sells_limit;
	}

	public static function upsells_limit()
	{
		$upsells_limit = nbcore_get_options('nbcore_upsells_limit');
		return $upsells_limit;
	}

//	public static function wc_review_form_heading()
//	{
//		$commenter = wp_get_current_commenter();
//
//		return array(
//			'title_reply'          => have_comments() ? __( 'Add a review', 'woocommerce' ) : sprintf( __( 'Be the first to review &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
//			'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
//			'title_reply_before'   => '<h2 id="reply-title" class="comment-reply-title">',
//			'title_reply_after'    => '</h2>',
//			'comment_notes_after'  => '',
//			'fields'               => array(
//				'author' => '<p class="comment-form-author">' . '<label for="author">' . esc_html__( 'Name', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
//					'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></p>',
//				'email'  => '<p class="comment-form-email"><label for="email">' . esc_html__( 'Email', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
//					'<input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></p>',
//			),
//			'label_submit'  => __( 'Submit', 'woocommerce' ),
//			'logged_in_as'  => '',
//			'comment_field' => '',
//		);
//	}
}