<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package nbcore
 */

// function test_options() {
//     $options = NBT_Core::get_options();

//     echo '<pre>';
//     print_r($options);
//     echo '</pre>';
// }

// function test_speed() {
//     $time_start = microtime(true);
//     $time_end = microtime(true);
//     $option = NBT_Core::get_options();
//     $a = $option['header_style'];
//     $time = $time_end - $time_start;

//     echo $time . '\n';
// }

// function test_speed_2() {
//     $time_start = microtime(true);
//     $time_end = microtime(true);
//     $option = NBT_Core::get_options();
//     $a = $option['header_fixed'];
//     $time = $time_end - $time_start;

//     echo $time ;
// }
// add_action('nb_core_before_header', 'test_speed');
// add_action('nb_core_before_header', 'test_speed_2');

function nbcore_get_header()
{

    // $options = NBT_Core::get_options();

    $header_style = nbcore_get_options('nbcore_header_style');

    get_template_part('template-parts/headers/' . $header_style);

//    switch ($header_style) {
//        case "mid-stack":
//            get_template_part('template-parts/headers/mid-stack');
//            break;
//        case "mid-inline":
//            get_template_part('template-parts/headers/mid-inline');
//            break;
//        case "left-inline":
//            get_template_part('template-parts/headers/left-inline');
//            break;
//        default:
//            get_template_part('template-parts/header/header-wrapper');
//    }
}

function nbcore_main_nav()
{
    $admin_url = get_admin_url() . 'customize.php?url=' . get_permalink() . '&autofocus%5Bsection%5D=menu_locations';

    if (has_nav_menu('primary')) {
        echo '<nav class="main-navigation" role="navigation">';

        wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu_class' => 'nb-navbar',
            'link_before' => '<span>',
            'link_after' => '</span>',
        ));

        echo '</nav>';
    } else {
        echo '<li><a href="' . $admin_url . '">' . esc_html__('Assign a menu here', 'core-wp') . '</a></li>';
    }
}

function nbcore_sub_menu()
{
    $admin_url = get_admin_url() . 'customize.php?url=' . get_permalink() . '&autofocus%5Bsection%5D=menu_locations';

    if (has_nav_menu('header-sub')) {
        echo '<nav class="sub-navigation" role="navigation">';

        wp_nav_menu(array(
            'theme_location' => 'header-sub',
            'menu_class' => 'nb-header-sub-menu',
            'link_before' => '<span>',
            'link_after' => '</span>',
        ));

        echo '</nav>';
    }

//    echo

}

function nbcore_get_nav_mobile()
{
    if (has_nav_menu('primary')) {
        echo '<nav class="main-mobile-navigation" role="navigation">';

        echo '<button class="mobile-toggle-button icon-menu"></button>';

        wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu_class' => 'nb-mobile-navbar',
            'link_before' => '<span>',
            'link_after' => '</span>',
        ));

        echo '</nav>';
    }
}

function nbcore_header_class()
{
    $classes = array();

    // $options = NBT_Core::get_options();

    $classes['header_style'] = nbcore_get_options('nbcore_header_style');

    if (nbcore_get_options('nbcore_header_fixed')) {
        $classes['header_fixed'] = 'fixed';
    }

    echo implode(' ', $classes);
}

function nbcore_header_woo_section()
{
    $header_style = nbcore_get_options('nbcore_header_style');

    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) { ?>
        <div class="header-account-wrap">
            <?php if (is_user_logged_in()):
                if ('left-inline' == $header_style):?>
                    <i class="icon-user-o"></i>
                <?php else: ?>
                    <span class="account-text"><?php esc_html_e('My Account', 'core-wp'); ?></span>
                <?php endif; ?>
                <div class="nb-account-dropdown">
                    <?php wc_get_template('myaccount/navigation.php'); ?>
                </div>
            <?php else: ?>
                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                   class="not-logged-in" title="<?php esc_attr_e('Login', 'core-wp'); ?>">
                    <?php if ('left-inline' == $header_style): ?>
                        <i class="icon-text-height"></i>
                    <?php else: ?>
                        <span class="account-text"><?php esc_html_e('Login', 'core-wp'); ?></span>
                    <?php endif; ?>
                </a>
            <?php endif; ?>
        </div>
        <div class="header-cart-wrap">
            <a class="nb-cart-section" href="<?php echo wc_get_cart_url(); ?>"
               title="<?php esc_attr_e('View cart', 'core-wp'); ?>">
                <i class="icon-shopping-bag-1"></i>
                <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            </a>
            <div class="mini-cart-section">
                <div class="mini-cart-wrap">
                    <?php woocommerce_mini_cart(); ?>
                </div>
            </div>
        </div>
    <?php }
}

function nbcore_search_section()
{
    $header_layout = nbcore_get_options('nbcore_header_style');

    if ('left-stack' == $header_layout) {
        get_search_form();
    } else {
        echo '<span class="icon-header-search"><i class="icon-search-1"></i></span>';
    }
}

function nbcore_get_site_logo()
{
    $logo = nbcore_get_options('nbcore_logo_upload');
    if ($logo) {
        $output = '<div class="main-logo img-logo">';
        $output .= '<a href="' . esc_url(home_url('/')) . '" title="' . get_bloginfo('description') . '">';
        $output .= '<img src="' . nbcore_get_options('nbcore_logo_upload') . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">';
        $output .= '</a>';
        $output .= '</div>';
    } else {
        $output = '<div class="main-logo text-logo">';
        $output .= '<a href="' . esc_url(home_url('/')) . '" title="' . get_bloginfo('description') . '">';
        $output .= get_bloginfo('name');
        $output .= '</a>';
        $output .= '</div>';
    }
    print($output);
}

function nbcore_featured_thumb()
{
    $blog_layout = nbcore_get_options('nbcore_blog_archive_layout');
    if (has_post_thumbnail()):
        if ('classic' == $blog_layout) {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
        } else {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'nbcore-masonry');
        }
        if ('classic' === $blog_layout):
            ?>
            <div class="entry-image">
                <a href="<?php the_permalink(); ?>">
                    <?php
                    printf('<img src="%1$s" title="%2$s" width="%3$s" height="%4$s" />',
                        $thumb[0],
                        esc_attr(get_the_title()),
                        $thumb[1],
                        $thumb[2]
                    );
                    ?>
                </a>
            </div>
        <?php else: ?>
            <div class="entry-image">
                <?php
                printf('<img src="%1$s" title="%2$s" width="%3$s" height="%4$s" />',
                    $thumb[0],
                    esc_attr(get_the_title()),
                    $thumb[1],
                    $thumb[2]
                );
                ?>
                <div class="image-mask">
                    <a href="<?php the_permalink(); ?>"><span><?php esc_html_e('View post &rarr;', 'core-wp'); ?></span></a>
                    <?php
                    $post = get_post();
                    $words = str_word_count(strip_tags($post->post_content));
                    $minutes = floor($words / 180);
                    if (1 <= $minutes) {
                        $estimated_time = $minutes . ' minute read';
                    } else {
                        $estimated_time = esc_html__('1 minutes read', 'core-wp');
                    }
                    echo '<div class="read-time"> ' . $estimated_time . '</div>';
                    ?>
                </div>

            </div>
        <?php endif;
    endif;
}

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function nbcore_posted_on()
{
    $html = '';

    if (nbcore_get_options('nbcore_blog_meta_date')) {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if (get_the_time('U') !== get_the_modified_time('U')) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf($time_string,
            esc_attr(get_the_date('c')),
            esc_html(get_the_date()),
            esc_attr(get_the_modified_date('c')),
            esc_html(get_the_modified_date())
        );

        $posted_on = sprintf(
            esc_html_x('%s', 'post date', 'core-wp'), $time_string);

        $html .= '<span class="posted-on">' . $posted_on . '</span>';
    };

    if ('masonry' !== nbcore_get_options('nbcore_blog_archive_layout')) {
        if (nbcore_get_options('nbcore_blog_meta_read_time')) {
            $post = get_post();
            $words = str_word_count(strip_tags($post->post_content));
            $minutes = floor($words / 180);
            if (1 <= $minutes) {
                $estimated_time = $minutes . ' minute read';
            } else {
                $estimated_time = esc_html__('1 minutes read', 'core-wp');
            }

            $html .= '<span class="read-time"> ' . $estimated_time . '</span>';
        }
    }

    if (!is_single()) {
        if (nbcore_get_options('nbcore_blog_meta_author')) {
            $byline = sprintf(
                esc_html_x('%s', 'post author', 'core-wp'),
                '<span class="author vcard"><a class="url fn n" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a></span>'
            );

            $html .= '<span class="byline"> by ' . $byline . '</span>';
        }
    }

    if ('' != $html) {
        echo '<div class="entry-meta">' . $html . '</div>';
    }
}

//function nbcore_archive_comments() {
//    echo '<span class="comments-link pull-right">' . comments_popup_link( __( 'Leave a comment', 'flatsome' ), __( '<strong>1</strong> Comment', 'flatsome' ), __( '<strong>%</strong> Comments', 'flatsome' ) ) . '</span>';
//}

function nbcore_get_categories()
{
    if (nbcore_get_options('nbcore_blog_meta_category')):?>
        <div class="entry-cat">
            <?php echo get_the_category_list(' '); ?>
        </div>
    <?php endif;
}

/**
 * Prints HTML with meta information for the categories, tags and comments.
 * TODO entry-footer wrap div rearrange
 */
function nbcore_get_tags()
{
    if (nbcore_get_options('nbcore_blog_meta_tag')) {
        // Hide category and tag text for pages.
        if ('post' === get_post_type()) {
            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list('', esc_html__(', ', 'core-wp'));
            if ($tags_list) {
                printf('<span class="tags-links icon-tags">' . esc_html__('%1$s', 'core-wp') . '</span>', $tags_list); // WPCS: XSS OK.
            }
        }
    }

}

function nbcore_get_excerpt()
{
    echo '<p class="entry-summary">';
    $limit = nbcore_get_options('nbcore_excerpt_length');
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . ' [...]';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    echo $excerpt;
    echo '</p>';
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function nbcore_categorized_blog()
{
    if (false === ($all_the_cool_cats = get_transient('nbcore_categories'))) {
        // Create an array of all the categories that are attached to posts.
        $all_the_cool_cats = get_categories(array(
            'fields' => 'ids',
            'hide_empty' => 1,
            // We only need to know if there is more than one category.
            'number' => 2,
        ));

        // Count the number of categories that are attached to the posts.
        $all_the_cool_cats = count($all_the_cool_cats);

        set_transient('nbcore_categories', $all_the_cool_cats);
    }

    if ($all_the_cool_cats > 1) {
        // This blog has more than 1 category so nbcore_categorized_blog should return true.
        return true;
    } else {
        // This blog has only 1 category so nbcore_categorized_blog should return false.
        return false;
    }
}

function nbcore_paging_nav()
{
    // Don't print empty markup if there's only one page.
    if ($GLOBALS['wp_query']->max_num_pages < 2) {
        return;
    }

    $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
    $pagenum_link = html_entity_decode(get_pagenum_link());
    $query_args = array();
    $url_parts = explode('?', $pagenum_link);

    if (isset ($url_parts[1])) {
        wp_parse_str($url_parts[1], $query_args);
    }

    $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
    $pagenum_link = trailingslashit($pagenum_link) . '%_%';

    $format = $GLOBALS['wp_rewrite']->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
    $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit('page/%#%', 'paged') : '?paged=%#%';

    // Set up paginated links.
    $links = paginate_links(array(
        'nbcore' => $pagenum_link,
        'format' => $format,
        'total' => $GLOBALS['wp_query']->max_num_pages,
        'current' => $paged,
        'mid_size' => 1,
        'add_args' => array_map('urlencode', $query_args),
        'prev_text' => wp_kses(__('<i class=\'icon-left-open\'></i>', 'core-wp'), array('i' => array('class' => array()))),
        'next_text' => wp_kses(__('<i class=\'icon-right-open\'></i>', 'core-wp'), array('i' => array('class' => array()))),
    ));

    if ($links) :

        ?>
        <nav class="navigation paging-navigation <?php echo nbcore_get_options('pagination_style'); ?>"
             role="navigation">
            <div class="pagination loop-pagination">
                <?php echo wp_kses($links, array(
                    'a' => array(
                        'href' => array(),
                        'class' => array()
                    ),
                    'i' => array(
                        'class' => array()
                    ),
                    'span' => array(
                        'class' => array()
                    )
                )); ?>
            </div><!--/ .pagination -->
        </nav><!--/ .navigation -->
        <?php
    endif;
}

function nbcore_share_social()
{
    $src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
    $style = nbcore_get_options('share_buttons_style');
    $position = nbcore_get_options('share_buttons_position');
    ?>
    <div class="nb-social-icons <?php echo $style . ' ' . $position; ?>">
        <a href="//www.facebook.com/sharer.php?u=<?php esc_url(the_permalink()); ?>" data-label="Facebook"
           onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"
           rel="nofollow" target="_blank" class="facebook" title="<?php esc_attr_e('Share on Facebook', 'core-wp'); ?>"><i
                    class="icon-facebook"></i></a>

        <a href="//twitter.com/share?url=<?php esc_url(the_permalink()); ?>"
           onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"
           rel="nofollow" target="_blank" class="twitter" title="<?php esc_attr_e('Share on Twitter', 'core-wp'); ?>"><i
                    class="icon-twitter"></i></a>

        <a href="//pinterest.com/pin/create/button/?url=<?php esc_url(the_permalink()); ?>&amp;media=<?php echo esc_attr($src[0]); ?>&amp;description=<?php the_title(); ?>"
           onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"
           rel="nofollow" target="_blank" class="pinterest" title="<?php esc_attr_e('Pin on Pinterest', 'core-wp'); ?>"><i
                    class="icon-pinterest"></i></a>

        <a href="//plus.google.com/share?url=<?php esc_url(the_permalink()); ?>" target="_blank" class="google-plus"
           onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"
           rel="nofollow" title="<?php esc_attr_e('Share on Google+', 'core-wp'); ?>"><i class="icon-gplus"></i></a>

        <a href="//www.linkedin.com/shareArticle?mini=true&url=<?php esc_url(the_permalink()); ?>&title=<?php the_title(); ?>"
           onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"
           rel="nofollow" target="_blank" class="linkedin" title="<?php esc_attr_e('Share on LinkedIn', 'core-wp'); ?>"><i
                    class="icon-linkedin"></i></a>
    </div>
    <?php
}

function nbcore_page_title()
{
    if (nbcore_get_options('show_title_section')) {
        if (is_home() || is_front_page()) {
            if (nbcore_get_options('home_page_title_section')) {
                echo '<div class="nb-page-title-wrap"><div class="container"><div class="nb-page-title"><h1>';
                esc_html_e('Home', 'core-wp');
                echo '</h1></div></div></div>';
            }
        } else {
            echo '<div class="nb-page-title-wrap"><div class="container"><div class="nb-page-title"><h1>';

            if (function_exists('is_shop') && is_shop()) {
                echo esc_html(nbcore_get_options('nbcore_shop_title'), 'core-wp');
            } elseif (function_exists('is_product_category') && is_product_category()) {
                echo single_cat_title();
            } elseif (function_exists('is_product_tag') && is_product_tag()) {
                echo single_tag_title();
            } elseif (is_post_type_archive()) {
                post_type_archive_title();
            } elseif (is_tax()) {
                single_term_title();
            } elseif (is_category()) {
                echo single_cat_title('', false);
            } elseif (is_archive()) {
                echo the_archive_title();
            } elseif (is_search()) {
                esc_html_e('Search Results', 'core-wp');
            } else {
                the_title();
            }

            echo '</h1>';

            if (function_exists('woocommerce_breadcrumb')) {
                if (nbcore_get_options('nbcore_wc_breadcrumb')) {
                    woocommerce_breadcrumb();
                }
            }

            echo '</div></div></div>';
        }
    }
}

/**
 * Flush out the transients used in nbcore_categorized_blog.
 */
function nbcore_category_transient_flusher()
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient('nbcore_categories');
}

add_action('edit_category', 'nbcore_category_transient_flusher');
add_action('save_post', 'nbcore_category_transient_flusher');

function nbcore_default_options($option)
{
    $default = array(
        'nbcore_blog_archive_layout' => 'classic',
        'nbcore_blog_sidebar' => 'right-sidebar',
        'nbcore_excerpt_only' => true,
        'nbcore_excerpt_length' => '35',
        'nbcore_blog_single_sidebar' => 'right-sidebar',
        'nbcore_color_scheme' => 'scheme_1',
        'nbcore_primary_color' => '#1e88e5',
        'nbcore_secondary_color' => '#fdd835',
        'nbcore_background_color' => '#ffffff',
        'nbcore_inner_background' => '#edf0f5',
        'nbcore_heading_color' => '#323232',
        'nbcore_body_color' => '#676c77',
        'nbcore_link_color' => '#000000',
        'nbcore_link_hover_color' => '#888888',
        'nbcore_divider_color' => '#e4e4e4',
        'nbcore_header_style' => 'mid-stack',
        'nbcore_logo_upload' => '',
        'nbcore_logo_width' => '200',
        'nbcore_header_fixed' => false,
        'header_bgcolor' => '#ffffff',
        'nbcore_blog_width' => '70',
        'nbcore_blog_meta_date' => true,
        'nbcore_blog_meta_read_time' => true,
        'nbcore_blog_meta_author' => true,
        'nbcore_blog_meta_category' => true,
        'nbcore_blog_meta_tag' => true,
        'nbcore_blog_sticky_sidebar' => false,
        'nbcore_blog_meta_align' => 'center',
        'show_title_section' => true,
        'nbcore_page_title_padding' => '75',
//        'nbcore_page_title_image' => '75',
        'nbcore_page_title_color' => '#323232',
        'body_font_family' => 'Noto Sans,400',
        'body_font_size' => '14',
        'heading_font_family' => 'Vidaloka,700',
        'heading_base_size' => '16',
        'subset_cyrillic' => false,
        'subset_greek' => false,
        'subset_vietnamese' => false,
        'nbcore_wc_breadcrumb' => true,
        'nbcore_wc_content_width' => '70',
        'nbcore_shop_title' => esc_html__('Shop', 'core-wp'),
        'nbcore_shop_action' => true,
        'nbcore_shop_sidebar' => 'right-sidebar',
        'nbcore_loop_columns' => 'three-columns',
        'nbcore_products_per_page' => '12',
        'nbcore_product_list' => 'grid-type',
        'nbcore_shop_content_width' => '70',
        'nbcore_grid_product_description' => false,
        'nbcore_pd_details_title' => true,
        'nbcore_pd_details_width' => '70',
        'nbcore_pd_details_sidebar' => 'right-sidebar',
        'nbcore_wc_sale' => 'style_1',
        'nbcore_pd_images_width' => '50',
        'nbcore_pd_thumb_pos' => 'bottom-thumb',
        'nbcore_pd_meta_layout' => 'left-images',
        'nbcore_pd_featured_autoplay' => false,
        'nbcore_info_style' => 'accordion-tabs',
        'nbcore_reviews_form' => 'full-width',
        'nbcore_reviews_round_avatar' => true,
        'nbcore_add_cart_style' => 'style-1',
        'nbcore_pd_show_social' => true,
        'nbcore_show_related' => true,
        'nbcore_pd_related_columns' => '3',
        'nbcore_show_upsells' => false,
        'nbcore_pd_upsells_columns' => '3',
        'nbcore_pb_background' => '#1e88e5',
        'nbcore_pb_background_hover' => '#1565C0',
        'nbcore_pb_text' => '#ffffff',
        'nbcore_pb_text_hover' => '#ffffff',
        'nbcore_pb_border' => '#1e88e5',
        'nbcore_pb_border_hover' => '#1565C0',
        'nbcore_sb_background' => 'transparent',
        'nbcore_sb_background_hover' => '#1e88e5',
        'nbcore_sb_text' => '#1e88e5',
        'nbcore_sb_text_hover' => '#ffffff',
        'nbcore_sb_border' => '#1e88e5',
        'nbcore_sb_border_hover' => '#1e88e5',
        'nbcore_button_padding' => '30',
        'nbcore_button_border_radius' => '0',
        'nbcore_button_border_width' => '2',
        'nbcore_cart_layout' => 'cart-layout-2',
        'nbcore_show_cross_sells' => true,
        'nbcore_cross_sells_per_row' => '4',
        'nbcore_cross_sells_limit' => '6',
        'home_page_title_section' => false,
        'nbcore_show_footer_top' => false,
        'nbcore_footer_top_layout' => 'layout-9',
        'nbcore_footer_top_color' => '#777777',
        'nbcore_footer_top_bg' => '#edf0f5',
        'nbcore_show_footer_bot' => false,
        'nbcore_footer_bot_layout' => 'layout-9',
        'nbcore_footer_bot_color' => '#777777',
        'nbcore_footer_bot_bg' => '#edf0f5',
        'nbcore_footer_abs_color' => '#edf0f5',
        'nbcore_footer_abs_bg' => '#1f1f1f',
        'nbcore_top_section_padding' => '10',
        'nbcore_middle_section_padding' => '20',
        'nbcore_bot_section_padding' => '30',
        'nbcore_header_top_bg' => '#282725',
        'nbcore_header_top_color' => '#e4e4e4',
        'nbcore_header_middle_bg' => '#ffffff',
        'nbcore_header_middle_color' => '#646464',
        'nbcore_header_bot_bg' => '#fff',
        'nbcore_header_bot_color' => '#646464',
        'nbcore_footer_top_heading' => '#323232',
        'nbcore_footer_bot_heading' => '#323232',
        'nbcore_blog_archive_comments' => true,
        'nbcore_blog_archive_summary' => true,
        'nbcore_blog_archive_post_style' => 'style-1',
        'nbcore_blog_single_title_position' => 'position-1',
        'nbcore_blog_single_show_thumb' => true,
        'nbcore_blog_single_title_size' => '50',
        'nbcore_blog_single_show_social' => true,
        'nbcore_blog_single_show_author' => true,
        'nbcore_blog_single_show_nav' => true,
        'nbcore_blog_single_show_comments' => true,
        'nbcore_page_title_size' => '50',
        'nbcore_footer_abs_padding' => '10',
        'share_buttons_style' => 'style-1',
        'share_buttons_position' => 'inside-content',
        'pagination_style' => 'style-1',
        'show_back_top' => true,
        'back_top_shape' => 'circle',
        'back_top_style' => 'light',
        'shop_sticky_sidebar' => false,
        'product_sticky_sidebar' => false,
        //meta
        'page_thumb' => 'no-thumb',
        'page_sidebar' => 'full-width',
        'page_content_width' => '70',
    );

    if (!empty($default[$option])) {
        return $default[$option];
    }
}

function nbcore_get_options($option)
{
    return get_theme_mod($option, nbcore_default_options($option));
}

function nbcore_get_post_meta($option)
{
    if ('' === get_post_meta(get_the_ID(), $option, true)) {
        return nbcore_default_options($option);
    } else {
        return get_post_meta(get_the_ID(), $option, true);
    }
}


function nbcore_blog_classes()
{
    $classes = array();

    $classes['sidebar'] = nbcore_get_options('nbcore_blog_sidebar');
    $classes['meta_align'] = 'meta-align-' . nbcore_get_options('nbcore_blog_meta_align');
    $classes['post_style'] = nbcore_get_options('nbcore_blog_archive_post_style');

    if ('masonry' === nbcore_get_options('nbcore_blog_archive_layout')) {
        $classes['masonry_columns'] = 'masonry-' . nbcore_get_options('nbcore_blog_masonry_columns') . '-columns';
    }


    echo implode(' ', $classes);
}

function nbcore_shop_classes()
{
    $classes = array();

    if ((is_shop() || is_product_category() || is_product_tag()) && 'grid-type' == nbcore_get_options('nbcore_product_list')) {
        $classes['shop_columns'] = nbcore_get_options('nbcore_loop_columns');
    }

    $classes['meta_layout'] = nbcore_get_options('nbcore_pd_meta_layout');

    if (function_exists('is_product') && is_product()) {
        $classes['nbcore_pd_thumb_pos'] = nbcore_get_options('nbcore_pd_thumb_pos');
    }

    if ('split' === nbcore_get_options('nbcore_reviews_form')) {
        $classes['nbcore_reviews_form'] = 'split-reviews-form';
    }

    if (nbcore_get_options('nbcore_reviews_round_avatar')) {
        $classes['nbcore_round_avatar'] = 'round-reviewer-avatar';
    }

    $classes['wc_tab_style'] = nbcore_get_options('nbcore_info_style');

    if (is_product()) {
        $classes['related_columns'] = 'related-' . nbcore_get_options('nbcore_pd_related_columns') . '-columns';
        $classes['upsells_columns'] = 'upsells-' . nbcore_get_options('nbcore_pd_upsells_columns') . '-columns';
    }

    echo implode(' ', $classes);
}

add_filter('woocommerce_add_to_cart_fragments', 'nb_header_add_to_cart_fragment');
function nb_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

    ?>
    <a class="nb-cart-section" href="<?php echo wc_get_cart_url(); ?>"
       title="<?php esc_attr_e('View cart', 'core-wp'); ?>">
        <i class="icon-shopping-bag-1"></i>
        <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
    </a>
    <?php

    $fragments['a.nb-cart-section'] = ob_get_clean();

    return $fragments;

}

add_filter('woocommerce_add_to_cart_fragments', 'nb_mini_cart_fragments');
function nb_mini_cart_fragments($fragments)
{

    ob_start();

    ?>
    <div class="mini-cart-wrap">
        <?php woocommerce_mini_cart(); ?>
    </div>
    <?php

    $fragments['.mini-cart-wrap'] = ob_get_clean();

    return $fragments;

}

function nbcore_back_to_top()
{
    $shape = nbcore_get_options('back_top_shape');
    $style = nbcore_get_options('back_top_style');
    echo '<div class="nb-back-to-top-wrap"><a id="back-to-top-button" class="' . esc_attr($shape) . ' ' . esc_attr($style) . '" href="#"><i class="icon-angle-up"></i></a></div>';
}
