<?php
class NBT_Customize_Control_Upload_Font extends WP_Customize_Control
{

    /**
     * Official control name.
     */
    public $type = 'upload-font';

    /**
     * Enqueue scripts and styles for the custom control.
     *
     * @access public
     */
    public function enqueue()
    {
        static $enqueued;

        if( !isset($enqueued) ) {
            wp_enqueue_script(
                'nb-upload-font',
                get_template_directory_uri() . '/assets/src/js/admin/upload-font.js',
                array('jquery'),
                '1.6.2',
                true
            );

            wp_localize_script('nb-upload-font', 'nbUpload', array(
                'form_heading' => esc_html__('Select Font', 'core-wp'),
                'button' => esc_html__('Select', 'core-wp')
            ));

            $enqueued = true;
        }

    }

    /**
     * Render the control.
     */
    public function render_content()
    {
        if(!empty($this->label)) {
            $button_label = $this->label;
        } else {
            $button_label = esc_html__('Upload font', 'core-wp');
        }
        ?>
        <div class="customize-control-content" id="nb-<?php echo esc_attr($this->type) ?>-<?php echo esc_attr($this->id) ?>">
            <div class="preview-font">
                <?php esc_html_e( 'Lorem Ipsum dolor sit amet', 'core-wp' ); ?>
            </div>
            <div class="nb-custom-fonts">
                <button type="button" class="button upload-font"><?php print($button_label); ?></button>
                <button type="button" class="button remove-font"><?php esc_html_e( 'Remove Font', 'wr-nitro' ); ?></button>
                <input type="hidden" class="font-holder" id="input_<?php echo $this->id; ?>" value="<?php echo $this->value(); ?>" <?php $this->link(); ?>>
            </div>
        </div>
        <?php
    }
}