<?php

/**
 * Slider custom control
 *
 * @since 1.0
 */
class NBT_Customize_Control_Slider extends WP_Customize_Control {
    public $type = 'slider';

    public function enqueue()
    {
        static $enqueued;
        
        if(!isset($enqueued)) {
            wp_enqueue_script('jquery-ui-slider');

            $enqueued = true;
        }
    }


    //TODO separate the calculate padding
    public function render_content()
    {
        // $unit = $this->choices['unit'] ? $this->choices['unit'] : '';
        if(!empty($this->choices['unit'])) {
            $unit = $this->choices['unit'];
        } else {
            $unit = '';
        }
        ?>
        <div class="customize-control-content" id="nb-<?php echo esc_attr($this->type)?>-<?php echo esc_attr($this->id)?>">
            <?php if( !empty($this->label) ): ?>
            <span class="customize-control-title">
                <?php echo esc_html($this->label); ?>						
            </span>
            <?php endif; ?>
            <?php if( !empty($this->description) ): ?>
            <span class="description customize-control-description">
                <?php echo esc_html($this->description); ?>            
            </span>
            <?php endif; ?>
            <input type="text" id="input_<?php echo $this->id; ?>" value="<?php echo $this->value(); ?>" <?php $this->link(); ?> style="display: none"/>
            <div id="slider_<?php echo esc_attr($this->id)?>" class="nb-slider"></div>
        </div>
        <script>
            jQuery(document).ready(function($) {
                var tooltip = $('<span id="tooltip"><?php echo intval($this->value()) . $unit; ?></span>');  
                $('#slider_<?php echo esc_attr($this->id)?>').slider({
                    range : "min",
                    value : <?php echo intval( $this->value() ); ?>,
                    min   : <?php echo intval( $this->choices['min'] ); ?>,
                    max   : <?php echo intval( $this->choices['max'] ); ?>,
                    step  : <?php echo intval( $this->choices['step'] ); ?>,
                    slide : function(e, ui) { 
                        $('#input_<?php echo $this->id; ?>').val(ui.value).keyup();
                        tooltip.text(ui.value + "<?php echo $unit; ?>");                        
                    }
                }).find(".ui-slider-handle").append(tooltip);                
            });
        </script>
        <?php
    }
}