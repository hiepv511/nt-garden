<?php
class NBT_Customize_Control_Font_Type extends WP_Customize_Control
{

    /**
     * Declare the control type.
     *
     * @access public
     * @var string
     */
    public $type = 'font-type';

    /**
     * Render the control to be displayed in the Customizer.
     */
    public function render_content()
    {
        $multi_values = !is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value(); ?>

        <div class="customize-control-content" id="nb-<?php echo esc_attr($this->type)?>-<?php echo esc_attr($this->id)?>">
        <?php
        if( !empty($this->label) ): ?>
            <span class="customize-control-title">
                <?php echo esc_html($this->label); ?>
            </span>
        <?php endif; ?>
        <?php if( !empty($this->description) ): ?>
            <span class="description customize-control-description">
                <?php echo esc_html($this->description); ?>
            </span>
        <?php endif; ?>

            <ul class="customize-control-checkbox-multiple">

                <li>
                    <label>
                        <input type="checkbox" name="italic" value="italic" <?php checked( in_array( 'italic', $multi_values ) ); ?> />
                        <i class="icon-italic"></i>
                    </label>
                </li>
                <li>
                    <label>
                        <input type="checkbox" name="underline" value="underline" <?php checked( in_array( 'underline', $multi_values ) ); ?> />
                        <i class="icon-underline"></i>
                    </label>
                </li>
                <li>
                    <label>
                        <input type="checkbox" name="uppercase" value="uppercase" <?php checked( in_array( 'uppercase', $multi_values ) ); ?> />
                        <i class="icon-text-height"></i>
                    </label>
                </li>

                <li>
                    <select name="weight">
                        <option value="400" <?php selected(end($multi_values), 400); ?>>Regular</option>
                        <option value="100" <?php selected(end($multi_values), 100); ?>>Thin</option>
                        <option value="300" <?php selected(end($multi_values), 300); ?>>Light</option>
                        <option value="700" <?php selected(end($multi_values), 700); ?>>Bold</option>
                        <option value="900" <?php selected(end($multi_values), 900); ?>>Ultra bold</option>
                    </select>
                </li>
            </ul>

            <input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $multi_values ) ); ?>" />
        </div>
        <?php
    }
}
?>