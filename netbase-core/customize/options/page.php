<?php

class NBT_Customize_Options_Pages
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Pages', 'core-wp'),
            'priority' => 18,
            'sections' => apply_filters('nbt_pages_array', array(
                'page_general' => array(
                    'title' => esc_html__('General', 'core-wp'),
                    'settings' => array(
//                        'page_sidebar' => array(
//                            'default' => 'full-width',
//                            'sanitize_callback' => ''
//                        ),
//                        'page_sticky_sidebar' => array(
//                            'default' => false,
//                            'sanitize_callback' => '',
//                        ),
//                        'page_thumb' => array(
//                            'default' => 'no-thumb',
//                            'sanitize_callback' => '',
//                        ),
                    ),
                    'controls' => array(
//                        'page_sidebar' => array(
//                            'label' => esc_html__('Sidebar', 'core-wp'),
//                            'section' => 'page_general',
//                            'type' => 'NBT_Customize_Control_Radio_Image',
//                            'choices' => array(
//                                'left-sidebar' => get_template_directory_uri() . '/assets/images/options/2cl.png',
//                                'full-width' => get_template_directory_uri() . '/assets/images/options/1c.png',
//                                'right-sidebar' => get_template_directory_uri() . '/assets/images/options/2cr.png',
//                            ),
//                        ),
//                        'page_sticky_sidebar' => array(
//                            'label' => esc_html__('Sticky sidebar', 'core-wp'),
//                            'section' => 'page_general',
//                            'type' => 'NBT_Customize_Control_Switch'
//                        ),
                    ),
                ),
            )),
        );
    }
}