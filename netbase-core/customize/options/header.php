<?php

class NBT_Customize_Options_Header
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Header Options', 'core-wp'),
            'description' => esc_html__('header description', 'core-wp'),
            'priority' => 11,
            'sections' => apply_filters('nbt_header_array', array(
                'header_presets' => array(
                    'title' => esc_html__('Presets', 'core-wp'),
                    'settings' => array(
                        'header_heading' => array(),
                        'nbcore_header_style' => array(
                            'default' => 'mid-stack',
                            'transport' => 'refresh',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'header_heading' => array(
                            'label' => esc_html__('Header style', 'core-wp'),
                            'description' => esc_html__('Quickly select a preset to change your header layout.', 'core-wp'),
                            'type' => 'NBT_Customize_Control_Heading',
                            'section' => 'header_presets',
                        ),
                        'nbcore_header_style' => array(
                            'section' => 'header_presets',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'mid-stack' => get_template_directory_uri() . '/assets/images/options/headers/mid-stack.png',
                                'mid-stack-2' => get_template_directory_uri() . '/assets/images/options/headers/mid-stack.png',
                                'mid-inline' => get_template_directory_uri() . '/assets/images/options/headers/mid-inline.png',
                                'left-inline' => get_template_directory_uri() . '/assets/images/options/headers/left-inline.png',
                                'left-stack' => get_template_directory_uri() . '/assets/images/options/headers/left-stack.png',
                                'left-stack-2' => get_template_directory_uri() . '/assets/images/options/headers/left-stack-2.png',
                            ),
                        ),
                    ),
                ),
                'header_general' => array(
                    'title' => esc_html__('Sections', 'core-wp'),
                    'settings' => array(
                        'nbcore_general_intro' => array(),
                        'nbcore_logo_upload' => array(
                            'default' => '',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_logo_width' => array(
                            'default' => '120',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_header_fixed' => array(
                            'default' => false,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_header_text_section' => array(
                            'default' => '',
                            'transport' => 'postMessage',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_header_top_intro' => array(),
                        'nbcore_top_section_padding' => array(
                            'default' => '10',
                            'transport' => 'postMessage',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_header_middle_intro' => array(),
                        'nbcore_middle_section_padding' => array(
                            'default' => '20',
                            'transport' => 'postMessage',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_header_bot_intro' => array(),
                        'nbcore_bot_section_padding' => array(
                            'default' => '30',
                            'transport' => 'postMessage',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_header_color_focus' => array(),
                    ),
                    'controls' => array(
                        'nbcore_general_intro' => array(
                            'label' => esc_html__('General', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_logo_upload' => array(
                            'label' => esc_html__('Site Logo', 'core-wp'),
                            'section' => 'header_general',
                            'description' => esc_html__('If you don\'t upload logo image, your site\'s logo will be the Site Title ', 'core-wp'),
                            'type' => 'WP_Customize_Upload_Control'
                        ),
                        'nbcore_logo_width' => array(
                            'label' => esc_html__('Logo Area Width', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '100',
                                'max' => '600',
                                'step' => '10',
                            ),
                        ),
                        'nbcore_header_fixed' => array(
                            'label' => esc_html__('Fixed header', 'core-wp'),
                            'section' => 'header_general',
                            'description' => esc_html__('test', 'core-wp'),
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_header_text_section' => array(
                            'label' => esc_html__('Text section', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'textarea',
                        ),
                        'nbcore_header_top_intro' => array(
                            'label' => esc_html__('Header topbar', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_top_section_padding' => array(
                            'label' => esc_html__('Top & bottom padding', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '45',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_header_middle_intro' => array(
                            'label' => esc_html__('Header Middle', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_middle_section_padding' => array(
                            'label' => esc_html__('Top & bottom padding', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '45',
                                'step' => '1'
                            ),
                        ),

                        'nbcore_header_bot_intro' => array(
                            'label' => esc_html__('Header bottom', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_bot_section_padding' => array(
                            'label' => esc_html__('Top & bottom padding', 'core-wp'),
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '45',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_header_color_focus' => array(
                            'section' => 'header_general',
                            'type' => 'NBT_Customize_Control_Focus',
                            'choices' => array(
                                'header_colors' => esc_html__('Edit color', 'core-wp'),
                            ),
                        ),
                    ),
                ),
            )),
        );
    }
}