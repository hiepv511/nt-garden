<?php
class NBT_Customize_Options_Blog
{
	public static function options()
	{
		return array(
			'title' => esc_html__('Blog', 'core-wp'),
			'priority' => 16,
			'sections' => apply_filters('nbt_blog_array', array(
			    'blog_general' => array(
			        'title' => esc_html__('General', 'core-wp'),
                    'settings' => array(
                        'nbcore_blog_layout_intro' => array(),
                        'nbcore_blog_sidebar' => array(
                            'default' => 'right-sidebar',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_width' => array(
                            'default' => '70',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_meta_intro' => array(),
                        'nbcore_blog_meta_date' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_meta_read_time' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_meta_author' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_meta_category' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_meta_tag' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_other_intro' => array(),
                        'nbcore_blog_sticky_sidebar' => array(
                            'default' => false,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_meta_align' => array(
                            'default' => 'center',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'nbcore_blog_layout_intro' => array(
                            'label' => esc_html__('Layout', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_blog_sidebar' => array(
                            'label' => esc_html__('Sidebar position', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-sidebar' => get_template_directory_uri() . '/assets/images/options/2cl.png',
                                'no-sidebar' => get_template_directory_uri() . '/assets/images/options/1c.png',
                                'right-sidebar' => get_template_directory_uri() . '/assets/images/options/2cr.png',
                            ),
                        ),
                        'nbcore_blog_width' => array(
                            'label' => esc_html__('Blog width', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '60',
                                'max' => '80',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_blog_meta_intro' => array(
                            'label' => esc_html__('Post meta', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_blog_meta_date' => array(
                            'label' => esc_html__('Show date', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_meta_read_time' => array(
                            'label' => esc_html__('Show time to read', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_meta_author' => array(
                            'label' => esc_html__('Show author', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_meta_category' => array(
                            'label' => esc_html__('Show categories', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_meta_tag' => array(
                            'label' => esc_html__('Show Tags', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_other_intro' => array(
                            'label' => esc_html__('Other', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_blog_sticky_sidebar' => array(
                            'label' => esc_html__('Sticky sidebar', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_meta_align' => array(
                            'label' => esc_html__('Meta align', 'core-wp'),
                            'section' => 'blog_general',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left' => get_template_directory_uri() . '/assets/images/options/meta-left.png',
                                'center' =>get_template_directory_uri() . '/assets/images/options/meta-center.png',
                                'right' => get_template_directory_uri() . '/assets/images/options/meta-right.png',
                            ),
                        ),
                    ),
                ),
				'blog_archive' => array(
					'title' => esc_html__('Blog Archive', 'core-wp'),
					'settings' => array(
						'nbcore_blog_archive_layout' => array(
							'default' => 'classic',
							'sanitize_callback' => ''
						),
                        'nbcore_blog_masonry_columns' => array(
                            'default' => '2',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_blog_archive_post_style' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => ''                            
                        ),
                        'nbcore_blog_archive_summary' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
						'nbcore_excerpt_only' => array(
							'default' => true,
							'sanitize_callback' => ''
						),
						'nbcore_excerpt_length' => array(
							'default' => '40',
							'sanitize_callback' => ''
						),
                        'nbcore_blog_archive_comments' => array(
                            'default' => true,
                            'sanitize_callback' => ''                        
                        ),
					),
					'controls' => array(
						'nbcore_blog_archive_layout' => array(
							'label' => esc_html__('Blog Archive Layout', 'core-wp'),
							'section' => 'blog_archive',
							'type' => 'NBT_Customize_Control_Radio_Image',
							'choices' => array(
								'classic' => get_template_directory_uri() . '/assets/images/options/classic.png',
								'masonry' => get_template_directory_uri() . '/assets/images/options/masonry.png',
							),
						),
                        'nbcore_blog_masonry_columns' => array(
                            'label' => esc_html__('Columns', 'core-wp'),
                            'section' => 'blog_archive',
                            'type' => 'select',
                            'choices' => array(
                                '2' => esc_html__('2', 'core-wp'),
                                '3' => esc_html__('3', 'core-wp'),
                            ),
                        ),
                        'nbcore_blog_archive_post_style' => array(
                            'label' => esc_html__('Post style', 'core-wp'),
                            'section' => 'blog_archive',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'style-1' => get_template_directory_uri() . '/assets/images/options/post-style-1.png',
                                'style-2' => get_template_directory_uri() . '/assets/images/options/post-style-2.png',
                            ),
                        ),
                        'nbcore_blog_archive_summary' => array(
                            'label' => esc_html__('Show Post summary', 'core-wp'),
                            'section' => 'blog_archive',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
						'nbcore_excerpt_only' => array(
							'label' => esc_html__('Show Excerpt Only', 'core-wp'),
							'section' => 'blog_archive',
							'type' => 'NBT_Customize_Control_Switch',
						),
						'nbcore_excerpt_length' => array(
							'label' => esc_html__('Excerpt Length', 'core-wp'),
							'section' => 'blog_archive',
							'type' => 'NBT_Customize_Control_Slider',
							'choices' => array(
								'min' => '20',
								'max' => '100',
								'step' => '1',
							),
						),
                        'nbcore_blog_archive_comments' => array(
                            'label' => esc_html__('Show Comments number', 'core-wp'),
                            'description' => esc_html__('test', 'core-wp'),
                            'section' => 'blog_archive',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
					),
				),
				'blog_single' => array(
					'title' => esc_html__('Blog Single', 'core-wp'),
					'settings' => array(
					    'nbcore_blog_single_title_intro' => array(),
						'nbcore_blog_single_title_position' => array(
						    'default' => 'position-1',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_blog_single_title_size' => array(
                            'default' => '50',
                            'transport' => 'postMessage',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_blog_single_layout_intro' => array(),
                        'nbcore_blog_single_show_thumb' => array(
                            'default' => true,
                            'sanitize_callback' => '',
                        ),
                        'nbcore_blog_single_show_social' => array(
                            'default' => true,
                            'sanitize_callback' => '',
                        ),
                        'nbcore_blog_single_show_author' => array(
                            'default' => true,
                            'sanitize_callback' => '',
                        ),
                        'nbcore_blog_single_show_nav' => array(
                            'default' => true,
                            'sanitize_callback' => '',
                        ),
                        'nbcore_blog_single_show_comments' => array(
                            'default' => true,
                            'sanitize_callback' => '',
                        ),
					),
					'controls' => array(
					    'nbcore_blog_single_title_intro' => array(
					        'label' => esc_html__('Post title', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
						'nbcore_blog_single_title_position' => array(
						    'label' => esc_html__('Post title style', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'position-1' => get_template_directory_uri() . '/assets/images/options/post-title-1.png',
                                'position-2' => get_template_directory_uri() . '/assets/images/options/post-title-2.png',
                            ),
                        ),
                        'nbcore_blog_single_title_size' => array(
                            'label' => esc_html__('Font size', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '16',
                                'max' => '70',
                                'step' => '1',
                            ),
                        ),
                        'nbcore_blog_single_layout_intro' => array(
                            'label' => esc_html__('Layout', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_blog_single_show_thumb' => array(
                            'label' => esc_html__('featured thumbnail', 'core-wp'),
                            'description' => esc_html__('Show featured thumbnail of this post on top of its content', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_single_show_social' => array(
                            'label' => esc_html__('Show social button', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_single_show_author' => array(
                            'label' => esc_html__('Show author info', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_single_show_nav' => array(
                            'label' => esc_html__('Show post navigation', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_blog_single_show_comments' => array(
                            'label' => esc_html__('Show post comments', 'core-wp'),
                            'section' => 'blog_single',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
					),
				),
			)),
		);
	}
}