<?php
class NBT_Customize_Options_Typography {
    public static function options()
    {
        return array(
            'title' => esc_html__('Typography', 'core-wp'),
            'priority' => 14,
            'settings' => array(
                'body_font_intro' => array(),
                'body_font_family' => array(
                    'default' => 'Noto Sans,400',
                    'sanitize_callback' => '',
                ),
                'body_font_size' => array(
                    'default' => '14',
                    'sanitize_callback' => '',
                ),
                'body_font_upload' => array(),
                'heading_font_intro' => array(),
                'heading_font_family' => array(
                    'default' => 'Vidaloka,700',
                    'sanitize_callback' => ''
                ),
                'heading_base_size' => array(
                    'default' => '16',
                    'sanitize_callback' => '',
                ),
                'heading_font_upload' => array(),
                'subset_intro' => array(),
                'subset_cyrillic' => array(
                    'default' => false,
                    'transport' => 'refresh',
                    'sanitize_callback' => '',
                ),
                'subset_greek' => array(
                    'default' => false,
                    'transport' => 'refresh',
                    'sanitize_callback' => '',
                ),
                'subset_vietnamese' => array(
                    'default' => false,
                    'transport' => 'refresh',
                    'sanitize_callback' => '',
                ),
                'font_color_focus' => array(),
            ),
            'controls' => array(
                'body_font_intro' => array(
                    'label' => esc_html__('Body Font', 'core-wp'),
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Heading',
                ),
                'body_font_family' => array(
                    'label'   => esc_html__( 'Font Family', 'core-wp' ),
                    'section' => 'typography',
                    'type'    => 'NBT_Customize_Control_Typography',
                    'choices' => array(
                        'weight' => true,
                    ),
                ),
                'body_font_size' => array(
                    'label' => esc_html__('Font Size', 'core-wp'),
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Slider',
                    'choices' => array(
                        'unit' => 'px',
                        'min' => '8',
                        'max' => '30',
                        'step' => '1',
                    ),
                ),
                'body_font_upload' => array(
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Upload_Font',
                ),
                'heading_font_intro' => array(
                    'label' => esc_html__('Heading Font', 'core-wp'),
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Heading',
                ),
                'heading_font_family' => array(
                    'label'   => esc_html__( 'Heading font', 'core-wp' ),
                    'section' => 'typography',
                    'type'    => 'NBT_Customize_Control_Typography',
                    'choices' => array(
                        'italic' => true,
                        'underline' => true,
                        'uppercase' => true,
                        'weight' => true,
                    ),
                ),
                'heading_base_size' => array(
                    'label' => esc_html__('Heading base size', 'core-wp'),
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Slider',
                    'choices' => array(
                        'unit' => 'px',
                        'min' => '10',
                        'max' => '40',
                        'step' => '1',
                    ),
                ),
                'heading_font_upload' => array(
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Upload_Font',
                ),
                'subset_intro' => array(
                    'label' => esc_html__('Font subset', 'core-wp'),
                    'description' => esc_html__('Turn these settings on if you have to support these scripts', 'core-wp'),
                    'section' => 'typography',
                    'type' => 'NBT_Customize_Control_Heading',
                ),
                'subset_cyrillic' => array(
                    'label'   => esc_html__( 'Cyrillic subset', 'core-wp' ),
                    'section' => 'typography',
                    'type'    => 'NBT_Customize_Control_Switch',
                ),
                'subset_greek' => array(
                    'label'   => esc_html__( 'Greek subset', 'core-wp' ),
                    'section' => 'typography',
                    'type'    => 'NBT_Customize_Control_Switch',
                ),
                'subset_vietnamese' => array(
                    'label'   => esc_html__( 'Vietnamese subset', 'core-wp' ),
                    'section' => 'typography',
                    'type'    => 'NBT_Customize_Control_Switch',
                ),
                'font_color_focus' => array(
                    'section' => 'typography',
                    'type'    => 'NBT_Customize_Control_Focus',
                    'choices' => array(
                        'type_color' => esc_html__('Edit font color', 'core-wp'),
                    ),
                ),
            ),
        );
    }
}