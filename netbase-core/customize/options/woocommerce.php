<?php
class NBT_Customize_Options_Woocommerce
{
    public static function options()
    {
        $arr = array(
            'title' => esc_html__('Woocommerce', 'core-wp'),
            'priority' => 15,
            'sections' => apply_filters('nbt_woocommerce_array', array(
                'product_category' => array(
                    'title' => esc_html__('Product category', 'core-wp'),
                    'settings' => array(
                        'nbcore_pa_title_intro' => array(),
                        'nbcore_shop_title' => array(
                            'default' => esc_html__('Shop', 'core-wp'),
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_wc_breadcrumb' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pa_layout_intro' => array(),
                        'nbcore_shop_content_width' => array(
                            'default' => '70',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_shop_sidebar' => array(
                            'default' => 'right-sidebar',
                            'sanitize_callback' => ''
                        ),
                        'shop_sticky_sidebar' => array(
                            'default' => false,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_product_list' => array(
                            'default' => 'grid-type',
                            'sanitize_callback' => '',
                        ),
                        'nbcore_loop_columns' => array(
                            'default' => 'three-columns',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pa_other_intro' => array(),
                        'nbcore_shop_banner' => array(
                            'default' => '',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_shop_action' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_products_per_page' => array(
                            'default' => '12',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_wc_sale' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_grid_product_description' => array(
                            'default' => false,
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'nbcore_pa_title_intro' => array(
                            'label' => esc_html__('Product category title', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_shop_title' => array(
                            'label' => esc_html__('Shop page title', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'text',
                        ),
                        'nbcore_wc_breadcrumb' => array(
                            'label' => esc_html__('Show breadcrumb ?', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pa_layout_intro' => array(
                            'label' => esc_html__('Product category layout', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_shop_content_width' => array(
                            'label' => esc_html__('Woocommerce content width', 'core-wp'),
                            'description' => esc_html__('This options also effect Cart page', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '60',
                                'max' => '80',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_shop_sidebar' => array(
                            'label' => esc_html__('Sidebar Layout', 'core-wp'),
                            'section' => 'product_category',
                            'description' => esc_html__('Sidebar Position for product category and shop page', 'core-wp'),
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-sidebar' => get_template_directory_uri() . '/assets/images/options/2cl.png',
                                'no-sidebar' => get_template_directory_uri() . '/assets/images/options/1c.png',
                                'right-sidebar' => get_template_directory_uri() . '/assets/images/options/2cr.png',
                            ),
                        ),
                        'shop_sticky_sidebar' => array(
                            'label' => esc_html__('Sticky Sidebar', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_product_list' => array(
                            'label' => esc_html__('Product List', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'grid-type' => get_template_directory_uri() . '/assets/images/options/grid.png',
                                'list-type' => get_template_directory_uri() . '/assets/images/options/list.png',
                            ),
                        ),
                        'nbcore_loop_columns' => array(
                            'label' => esc_html__('Products per row', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'two-columns' => get_template_directory_uri() . '/assets/images/options/2-columns.png',
                                'three-columns' => get_template_directory_uri() . '/assets/images/options/3-columns.png',
                                'four-columns' => get_template_directory_uri() . '/assets/images/options/4-columns.png',
                            ),
                        ),
                        'nbcore_pa_other_intro' => array(
                            'label' => esc_html__('Other', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_shop_banner' => array(
                            'label' => esc_html__('Shop Banner', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'WP_Customize_Cropped_Image_Control',
                            'flex_width'  => true,
                            'flex_height' => true,
                            'width' => 2000,
                            'height' => 1000,
                        ),
                        'nbcore_shop_action' => array(
                            'label' => esc_html__('Show shop action', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Switch'
                        ),
                        'nbcore_products_per_page' => array(
                            'label' => esc_html__('Products per Page', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'number',
                            'input_attrs' => array(
                                'min'   => 1,
                                'step'  => 1,
                            ),
                        ),
                        'nbcore_wc_sale' => array(
                            'label' => esc_html__('Choose sale tag style', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'select',
                            'choices' => array(
                                'style-1' => esc_html__('Style 1', 'core-wp'),
                                'style-2' => esc_html__('Style 2', 'core-wp'),
                            ),
                        ),
                        //TODO remove this
                        'nbcore_grid_product_description' => array(
                            'label' => esc_html__('Product Description', 'core-wp'),
                            'section' => 'product_category',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                    ),
                ),
                'product_details' => array(
                    'title' => esc_html__('Product details', 'core-wp'),
                    'settings' => array(
                        'nbcore_pd_layout_intro' => array(),
                        'nbcore_pd_details_title' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_details_width' => array(
                            'default' => '70',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_details_sidebar' => array(
                            'default' => 'right-sidebar',
                            'sanitize_callback' => ''
                        ),
                        'product_sticky_sidebar' => array(
                            'default' => false,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_meta_layout' => array(
                            'default' => 'left-images',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_add_cart_style' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_show_social' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_gallery_intro' => array(),
                        'nbcore_pd_images_width' => array(
                            'default' => '50',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_featured_autoplay' => array(
                            'default' => false,
                            'sanitize_callback' => '',
                        ),
                        'nbcore_pd_thumb_pos' => array(
                            'default' => 'bottom-thumb',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_info_tab_intro' => array(),
                        'nbcore_info_style' => array(
                            'default' => 'accordion-tabs',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_reviews_form' => array(
                            'default' => 'split',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_reviews_round_avatar' => array(
                            'default' => false,
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_other_products_intro' => array(),
                        'nbcore_show_upsells' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_upsells_columns' => array(
                            'default' => '3',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_upsells_limit' => array(
                            'default' => '6',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_show_related' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_pd_related_columns' => array(
                            'default' => '3',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'nbcore_pd_layout_intro' => array(
                            'label' => esc_html__('Layout', 'core-wp'),
                            'description' => esc_html__('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_pd_details_title' => array(
                            'label' => esc_html__('Enable Product title', 'core-wp'),
                            'description' => esc_html__('Default product title is not display if the Page title is showing. Enable this to displaying both.', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pd_details_width' => array(
                            'label' => esc_html__('Product details content width', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '60',
                                'max' => '80',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_pd_details_sidebar' => array(
                            'label' => esc_html__('Product details sidebar', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-sidebar' => get_template_directory_uri() . '/assets/images/options/2cl.png',
                                'no-sidebar' => get_template_directory_uri() . '/assets/images/options/1c.png',
                                'right-sidebar' => get_template_directory_uri() . '/assets/images/options/2cr.png',
                            ),
                        ),
                        'product_sticky_sidebar' => array(
                            'label' => esc_html__('Sticky sidebar', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pd_meta_layout' => array(
                            'label' => esc_html__('Product meta layout', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-images' => get_template_directory_uri() . '/assets/images/options/left-image.png',
                                'right-images' => get_template_directory_uri() . '/assets/images/options/right-image.png',
                                'wide' => get_template_directory_uri() . '/assets/images/options/wide.png',
                            ),
                        ),
                        'nbcore_add_cart_style' => array(
                            'label' => esc_html__('Add to cart input style', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                'style-1' => esc_html__('Style 1', 'core-wp'),
                                'style-2' => esc_html__('Style 2', 'core-wp'),
                            ),
                        ),
                        'nbcore_pd_show_social' => array(
                            'label' => esc_html__('Show social share?', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pd_gallery_intro' => array(
                            'label' => esc_html__('Product Gallery', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_pd_images_width' => array(
                            'label' => esc_html__('Product images width', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '30',
                                'max' => '60',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_pd_featured_autoplay' => array(
                            'label' => esc_html__('Featured Images Autoplay', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pd_thumb_pos' => array(
                            'label' => esc_html__('Small thumb position', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'bottom-thumb' => get_template_directory_uri() . '/assets/images/options/bottom-thumb.png',
                                'left-thumb' => get_template_directory_uri() . '/assets/images/options/left-thumb.png',
                                'inside-thumb' => get_template_directory_uri() . '/assets/images/options/inside-thumb.png',
                                'right-dots' => get_template_directory_uri() . '/assets/images/options/right-dots.png',
                            ),
                        ),
                        'nbcore_pd_info_tab_intro' => array(
                            'label' => esc_html__('Information tab', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_info_style' => array(
                            'label' => esc_html__('Tab style', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                'horizontal-tabs' => esc_html__('Horizontal', 'core-wp'),
                                'accordion-tabs' => esc_html__('Accordion', 'core-wp'),
                            ),
                        ),
                        'nbcore_reviews_form' => array(
                            'label' => esc_html__('Reviews form style', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                'split' => esc_html__('Split', 'core-wp'),
                                'full-width' => esc_html__('Full Width', 'core-wp'),
                            ),
                        ),
                        'nbcore_reviews_round_avatar' => array(
                            'label' => esc_html__('Round reviewer avatar', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_other_products_intro' => array(
                            'label' => esc_html__('Related & Cross-sells products', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Heading',
                        ),
                        'nbcore_show_upsells' => array(
                            'label' => esc_html__('Show upsells products?', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pd_upsells_columns' => array(
                            'label' => esc_html__('Upsells Products per row', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                '2' => esc_html__('2 Products', 'core-wp'),
                                '3' => esc_html__('3 Products', 'core-wp'),
                                '4' => esc_html__('4 Products', 'core-wp'),
                            ),
                        ),
                        'nbcore_upsells_limit' => array(
                            'label' => esc_html__('Upsells Products limit', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'number',
                            'input_attrs' => array(
                                'min' => '2',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_show_related' => array(
                            'label' => esc_html__('Show related product?', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_pd_related_columns' => array(
                            'label' => esc_html__('Related Products per row', 'core-wp'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                '2' => esc_html__('2 Products', 'core-wp'),
                                '3' => esc_html__('3 Products', 'core-wp'),
                                '4' => esc_html__('4 Products', 'core-wp'),
                            ),
                        ),
                    ),
                ),
                'other_wc_pages' => array(
                    'title' => esc_html__('Other Pages', 'core-wp'),
                    'settings' => array(
                        'nbcore_cart_intro' => array(),
                        'nbcore_cart_layout' => array(
                            'default' => 'cart-layout-1',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_show_to_shop' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_show_cross_sells' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_cross_sells_per_row' => array(
                            'default' => '4',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_cross_sells_limit' => array(
                            'default' => '6',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'nbcore_cart_intro' => array(
                            'label' => esc_html__('Cart', 'core-wp'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBT_Customize_Control_Heading'
                        ),
                        'nbcore_cart_layout' => array(
                            'label' => esc_html__('Cart page layout', 'core-wp'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'cart-layout-1' => get_template_directory_uri() . '/assets/images/options/cart-style-1.png',
                                'cart-layout-2' => get_template_directory_uri() . '/assets/images/options/cart-style-2.png',
                            ),
                        ),
                        'nbcore_show_to_shop' => array(
                            'label' => esc_html__('Show Continue shopping button', 'core-wp'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_show_cross_sells' => array(
                            'label' => esc_html__('Show cross sells', 'core-wp'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBT_Customize_Control_Switch'
                        ),
                        'nbcore_cross_sells_per_row' => array(
                            'label' => esc_html__('Products per row', 'core-wp'),
                            'section' => 'other_wc_pages',
                            'type' => 'select',
                            'choices' => array(
                                '3' => esc_html__('3 products', 'core-wp'),
                                '4' => esc_html__('4 products', 'core-wp'),
                                '5' => esc_html__('5 products', 'core-wp'),
                            ),
                        ),
                        'nbcore_cross_sells_limit' => array(
                            'label' => esc_html__('Cross sells Products limit', 'core-wp'),
                            'section' => 'other_wc_pages',
                            'type' => 'number',
                            'input_attrs' => array(
                                'min' => '3',
                                'step' => '1'
                            ),
                        ),
                    ),
                ),
            )),
        );

        return $arr;
    }
}