<?php

class NBT_Customize_Options_Elements
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Elements', 'core-wp'),
            'priority' => 12,
            'sections' => apply_filters('nbt_elements_array', array(
                'title_section_element' => array(
                    'title' => esc_html__('Title section', 'core-wp'),
                    'settings' => array(
                        'show_title_section' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'home_page_title_section' => array(
                            'default' => false,
                            'sanitize_callback' => ''
                        ),
                        'nbcore_page_title_size' => array(
                            'default' => '50',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_page_title_padding' => array(
                            'default' => '75',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        // TODO postMessage this
                        'nbcore_page_title_image' => array(
                            'default' => '',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_page_title_color_focus' => array(),
                    ),
                    'controls' => array(
                        'show_title_section' => array(
                            'label' => esc_html__('Show title section', 'core-wp'),
                            'section' => 'title_section_element',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'home_page_title_section' => array(
                            'label' => esc_html__('Show Homepage title', 'core-wp'),
                            'description' => esc_html__('Turn this off to not display the title section for only homepage', 'core-wp'),
                            'section' => 'title_section_element',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'nbcore_page_title_size' => array(
                            'label' => esc_html__('Font size', 'core-wp'),
                            'section' => 'title_section_element',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '16',
                                'max' => '70',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_page_title_padding' => array(
                            'label' => esc_html__('Padding top and bottom', 'core-wp'),
                            'section' => 'title_section_element',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '15',
                                'max' => '105',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_page_title_image' => array(
                            'label' => esc_html__('Image Background', 'core-wp'),
                            'section' => 'title_section_element',
                            'type' => 'WP_Customize_Cropped_Image_Control',
                            'flex_width'  => true,
                            'flex_height' => true,
                            'width' => 2000,
                            'height' => 1000,
                        ),
                        'nbcore_page_title_color_focus' => array(
                            'section' => 'title_section_element',
                            'type' => 'NBT_Customize_Control_Focus',
                            'choices' => array(
                                'other_colors' => esc_html__('Edit color', 'core-wp')
                            ),
                        ),
                    ),
                ),
                'button_element' => array(
                    'title' => esc_html__('Button', 'core-wp'),
                    'settings' => array(
                        'nbcore_button_padding' => array(
                            'default' => '30',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_button_border_radius' => array(
                            'default' => '0',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'nbcore_button_border_width' => array(
                            'default' => '2',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'nbcore_button_padding' => array(
                            'label' => esc_html__('Padding left & right', 'core-wp'),
                            'section' => 'button_element',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '5',
                                'max' => '60',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_button_border_radius' => array(
                            'label' => esc_html__('Border Radius', 'core-wp'),
                            'section' => 'button_element',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '50',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_button_border_width' => array(
                            'label' => esc_html__('Border Width', 'core-wp'),
                            'section' => 'button_element',
                            'type' => 'NBT_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '1',
                                'max' => '10',
                                'step' => '1'
                            ),
                        ),
                    ),
                ),
                'share_buttons_element' => array(
                    'title' => esc_html__('Social share button', 'core-wp'),
                    'settings' => array(
                        'share_buttons_style' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => ''
                        ),
                        'share_buttons_position' => array(
                            'default' => 'inside-content',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'share_buttons_style' => array(
                            'label' => esc_html__('Style','core-wp'),
                            'section' => 'share_buttons_element',
                            'type' => 'select',
                            'choices' => array(
                                'style-1' => esc_html__('Style 1', 'core-wp'),
                                'style-2' => esc_html__('Style 2', 'core-wp'),
                            ),
                        ),
                        'share_buttons_position' => array(
                            'label' => esc_html__('Buttons position','core-wp'),
                            'section' => 'share_buttons_element',
                            'type' => 'NBT_Customize_Control_Radio_Image',
                            'choices' => array(
                                'inside-content' => get_template_directory_uri() . '/assets/images/options/ss-inside.png',
                                'floating' => get_template_directory_uri() . '/assets/images/options/ss-floating.png',
                            ),
                        ),
                    ),
                ),
                'pagination_element' => array(
                    'title' => esc_html__('Pagination', 'core-wp'),
                    'settings' => array(
                        'pagination_style' => array(
                            'default' => 'pagination-style-1',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        //TODO Radio image this
                        'pagination_style' => array(
                            'label' => esc_html__('Style', 'core-wp'),
                            'section' => 'pagination_element',
                            'type' => 'select',
                            'choices' => array(
                                'pagination-style-1' => esc_html__('Style 1','core-wp'),
                                'pagination-style-2' => esc_html__('Style 2','core-wp'),
                            ),
                        ),
                    ),
                ),
                'back_top_element' => array(
                    'title' => esc_html__('Back to top', 'core-wp'),
                    'settings' => array(
                        'show_back_top' => array(
                            'default' => true,
                            'sanitize_callback' => ''
                        ),
                        'back_top_shape' => array(
                            'default' => 'circle',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                        'back_top_style' => array(
                            'default' => 'light',
                            'transport' => 'postMessage',
                            'sanitize_callback' => ''
                        ),
                    ),
                    'controls' => array(
                        'show_back_top' => array(
                            'label' => esc_html__('Show button', 'core-wp'),
                            'section' => 'back_top_element',
                            'type' => 'NBT_Customize_Control_Switch',
                        ),
                        'back_top_shape' => array(
                            'label' => esc_html__('Show button', 'core-wp'),
                            'section' => 'back_top_element',
                            'type' => 'select',
                            'choices' => array(
                                'circle' => esc_html__('Circle','core-wp'),
                                'square' => esc_html__('Square','core-wp'),
                            ),
                        ),
                        'back_top_style' => array(
                            'label' => esc_html__('Show button', 'core-wp'),
                            'section' => 'back_top_element',
                            'type' => 'select',
                            'choices' => array(
                                'light' => esc_html__('Light','core-wp'),
                                'dark' => esc_html__('Dark','core-wp'),
                            ),
                        ),
                    ),
                ),
            )),
        );
    }
}