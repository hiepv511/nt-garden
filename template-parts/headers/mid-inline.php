<div class="top-section-wrap">
    <div class="container">
        <div class="top-section">
            <?php $text_section_content = nbcore_get_options('nbcore_header_text_section');
            if($text_section_content):
            ?>
            <div class="text-section">
                <?php echo esc_html($text_section_content); ?>
            </div>
            <?php
            endif; ?>
            <div class="icon-header-section">
                <div class="icon-header-wrap">
                <?php
                nbcore_header_woo_section();
                nbcore_search_section();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <div class="flex-section equal-section flex-end">
                <?php nbcore_main_nav(); ?>
            </div>
            <div class="flex-section">
                <?php nbcore_get_site_logo(); ?>
            </div>
            <div class="flex-section equal-section">
                <?php nbcore_sub_menu();?>
            </div>
        </div>
    </div>
</div>