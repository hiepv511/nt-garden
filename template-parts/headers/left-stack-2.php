<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <?php nbcore_get_site_logo(); ?>
            <div class="main-nav-wrap">
                <?php nbcore_sub_menu(); ?>
                <div class="icon-header-wrap">
                    <?php nbcore_header_woo_section(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bot-section-wrap">
    <div class="container">
        <div class="bot-section">
            <?php nbcore_main_nav(); ?>
        </div>
    </div>
</div>
