<!--Site main navbar-->
<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <div class="flex-section equal-section">
                <?php nbcore_main_nav(); ?>
            </div>
            <div class="flex-section">
                <?php nbcore_get_site_logo(); ?>
            </div>
            <div class="flex-section equal-section flex-end">
                <div class="icon-header-section">
                    <div class="icon-header-wrap">
                        <?php
                        nbcore_header_woo_section();
                        nbcore_search_section();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End site main navbar-->
