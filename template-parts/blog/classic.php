<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nbcore
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php nbcore_featured_thumb(); ?>
	<div class="entry-content">
        <?php nbcore_posted_on();
		the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
        nbcore_get_categories();
		if(nbcore_get_options('nbcore_blog_archive_summary')):
		?>
		<div class="entry-text">
			<?php
			if(nbcore_get_options('nbcore_excerpt_only')) {
				nbcore_get_excerpt();
				echo '<div class="read-more-link"><a class="bt-4 nb-secondary-button" href="' . get_permalink() . '">' . esc_html__('View post', 'core-wp') . '<span>&rarr;</span></a></div>';
			} else {
				the_content( sprintf(
				/* translators: %s: Name of current post. */
					wp_kses( esc_html__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'core-wp' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'core-wp' ),
					'after'  => '</div>',
				) );
			}			
			?>
		</div>
		<?php endif; ?>
        <div class="entry-footer">
		    <?php nbcore_get_tags(); ?>
		    <?php if(nbcore_get_options('nbcore_blog_archive_comments')):?>
	            <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	                <span class="comments-link"><?php comments_popup_link( esc_html__( 'Leave a comment', 'core-wp' ), esc_html__( '<strong>1</strong> Comment', 'core-wp' ), esc_html__( '<strong>%</strong> Comments', 'core-wp' ) ); ?></span>
	            <?php endif; ?>
        	<?php endif;?>
        </div>
	</div>
	
</article><!-- #post-## -->
