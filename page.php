<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nbcore
 */

get_header();
if(get_post_meta(get_the_ID(), 'page_title_section')) {
    nbcore_page_title();
}
$page_sidebar = nbcore_get_post_meta( 'page_sidebar');
?>
	
	<div class="container">
		<div class="row">
			<div id="primary" class="content-area page-<?php echo esc_attr($page_sidebar); ?>">
				<main id="main" class="site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

				</main><!-- #main -->
			</div><!-- #primary -->
			<?php
            if('full-width' !== $page_sidebar) {
                get_sidebar();
            }
			?>
		</div>
	</div>

<?php
get_footer();
