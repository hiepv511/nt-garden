(function( $ ) {

    $(document).ready(function() {
        $('.nb-select').chosen();

        $('.focus-section').click(function(e) {
            e.preventDefault();
            var s = $(this).data('section');
            wp.customize.section(s).focus();
            a(".accordion-section-content").css("margin-top", "0")
        });

        $('.alpha-color-control').each(function(index, element) {
            var wrapElement = $(element).closest('.color-control-wrap');
            $(element).spectrum({
                color: $(element).val(),
                showInput: true,
                showInitial: true,
                allowEmpty: true,
                showAlpha: true,
                clickoutFiresChange: true,
                preferredFormat: "hex",
                show: function() {
                    // var initColor = $(element).spectrum("get").toHexString();
                    wrapElement.find('#default').on("click", function(e) {
                        e.preventDefault();
                        var defaultColor = $(this).attr("data-default-color");

                        $(element).spectrum("set", defaultColor);
                        wrapElement.css('background-color', defaultColor);
                        wrapElement.find('#color-placeholder').text(defaultColor);
                        $(element).trigger('change');
                    });
                    // $(element).find('.sp-cancel').on('click', function() {
                    //     console.log(initColor);
                    //     $(element).spectrum("set", initColor);
                    //     $(element).trigger('change');
                    // })
                },
                change: function() {
                    var color = $(element).spectrum("get").toHexString();
                    wrapElement.find('#color-placeholder').text(color);
                    wrapElement.css('background-color', color);
                },
                move: function(color) {
                    $(element).spectrum("set", color);
                    var color = $(element).spectrum("get").toHexString();
                    wrapElement.find('#color-placeholder').text(color);
                    wrapElement.css('background-color', color);
                    $(element).trigger('change');
                },
                hide: function(color) {
                    $(element).spectrum("set", color);
                    var color = $(element).spectrum("get").toHexString();
                    wrapElement.find('#color-placeholder').text(color);
                    wrapElement.css('background-color', color);
                    $(element).trigger('change');
                }
            });
        });
    });

})( jQuery );
