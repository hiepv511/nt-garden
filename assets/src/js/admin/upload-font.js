(function($) {
    $(document).ready( function() {
        $('.nb-custom-fonts .upload-font').on( 'click', function() {
            // Accepts an optional object hash to override default values.
            var self = $(this);
            var frame = new wp.media({
                title: nbUpload.form_heading,
                multiple: true,
                library: {
                    type: ["application/vnd.ms-fontobject", "application/x-font-opentype", "application/x-font-ttf", "application/font-woff", "application/font-woff2"],
                },
                button: {
                    text: nbUpload.button,
                },
            });

            frame.open();

            var resultArray = [];

            frame.on('select', function() {
                var fonts = frame.state().get('selection').toJSON();
                // console.log(typeof(fonts));
                var i;

                for(i = 0; i < fonts.length; i++) {
                    resultArray.push(fonts[i].url);
                }
                // wp.customize.setting(frame.setting)
                // console.log(frame.setting);
                self.closest('.nb-custom-fonts').find('.font-holder').val(resultArray.toString());
            });
        });
    });
})(jQuery);
