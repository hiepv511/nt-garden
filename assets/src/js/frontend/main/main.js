(function ($) {

    var midStackEl = $('.site-header.mid-stack.fixed .bot-section-wrap');
    if(midStackEl.length > 0) {
        var sticky = new Waypoint.Sticky({
            element: $('.site-header.mid-stack.fixed .bot-section-wrap')[0],
        })
    }

    var midInlineEl = $('.site-header.mid-inline.fixed .middle-section-wrap');
    if(midInlineEl.length > 0) {
        var sticky = new Waypoint.Sticky({
            element: $('.site-header.mid-inline.fixed .middle-section-wrap')[0],
        })
    }

    var leftInlineEl = $('.site-header.left-inline.fixed .middle-section-wrap');
    if(leftInlineEl.length > 0) {
        var sticky = new Waypoint.Sticky({
            element: $('.site-header.left-inline.fixed .middle-section-wrap')[0],
        })
    }

    var leftStackEl = $('.site-header.left-stack.fixed .bot-section-wrap');
    if(leftStackEl.length > 0) {
        var sticky = new Waypoint.Sticky({
            element: $('.site-header.left-stack.fixed .bot-section-wrap')[0],
        })
    }

    // var stickyHeaderWaypoint = new Waypoint({
    //     element: $('.site-header.fixed .main-nav-wrap'),
    //     handler: function(direction) {
    //         $('.site-header.fixed .main-nav-wrap').slideDown('fast')
    //     }
    // });

    $('.widget_nav_menu .menu-item-has-children').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).find('.sub-menu').slideToggle('fast');
    });

    $(document).on({
        mouseenter: function () {
            $(this).find('.sub-menu').stop().first().fadeIn(200);
        },
        mouseleave: function () {
            $(this).find('.sub-menu').stop().fadeOut(200);
        }
    }, '.nb-navbar .menu-item-has-children, .nb-header-sub-menu .menu-item-has-children');

    $('.nb-mobile-navbar .menu-item-has-children > a').append('<span class="icon-angle-down"></span>');

    $('.nb-mobile-navbar .menu-item-has-children .icon-angle-down').on('click', function (e) {
        e.preventDefault();
        $(this).parent().siblings('.sub-menu').slideToggle();
    });

    $('.mobile-toggle-button').on('click', function () {
        $(this).next().find('.nb-mobile-navbar').slideToggle()
    });

    $('.blog .masonry').isotope({
        itemSelector: '.post',
    });

    var d = 0;
    var $numbertype = null;

    $(".quantity-plus, .quantity-minus").mousedown(function () {
        $el = $(this).closest('.nb-quantity').find('.qty');
        $numbertype = parseInt($el.val());
        d = $(this).is(".quantity-minus") ? -1 : 1;
        $el.val($numbertype + d);
    });

    if (jQuery().magnificPopup) {
        $('.featured-gallery').magnificPopup({
            delegate: 'img',
            type: 'image',
            gallery: {
                enabled: true
            },
            callbacks: {
                elementParse: function (item) {
                    item.src = item.el.attr('src');
                }
            }
        });
    }

    var $upsells = $('.upsells .products');
    var $upsellsCells = $upsells.find('.product');

    if ($upsellsCells.length <= nb.upsells_columns) {
        $upsells.addClass('hiding-nav-ui');
    }

    var $related = $('.related .products');
    var $relatedCells = $related.find('.product');

    if ($relatedCells.length <= nb.related_columns) {
        $related.addClass('hiding-nav-ui');
    }

    var $crossSells = $('.cross-sells .products');
    var $crossSellsCells = $crossSells.find('.product');

    if ($crossSellsCells.length <= nb.cross_sells_columns) {
        $crossSells.addClass('hiding-nav-ui');
    }

    if (jQuery().accordion) {
        $('.shop-main.accordion-tabs .wc-tabs').accordion({
            header: ".accordion-title-wrap",
            heightStyle: "content",
        });
    }

    $('.header-cart-wrap').on({
        mouseenter: function () {
            $(this).find('.mini-cart-section').stop().fadeIn('fast');
        },
        mouseleave: function () {
            $(this).find('.mini-cart-section').stop().fadeOut('fast');
        }
    });

    $('.header-account-wrap').on({
        mouseenter: function () {
            $(this).find('.nb-account-dropdown').stop().fadeIn('fast');
        },
        mouseleave: function () {
            $(this).find('.nb-account-dropdown').stop().fadeOut('fast');
        }
    });

    $(document.body).on('added_to_cart', function () {
        $('.mini-cart-section')
            .fadeIn('fast')
            .delay(3500)
            .fadeOut('fast');
    });


    var $sticky = $('.sticky-wrapper.sticky-sidebar');

    if($sticky.length > 0) {
        $($sticky).stick_in_parent({
            offset_top: 45
        });

        $(window).on('resize', function() {
            $($sticky).trigger('sticky_kit:detach');
        });
    }

    if ($('#back-to-top-button').length) {
        var scrollTrigger = 500; // px
        var backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top-button').addClass('show');
                } else {
                    $('#back-to-top-button').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top-button').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

})(jQuery);