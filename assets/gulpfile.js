var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rtlcss = require('gulp-rtlcss');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var cssnano = require('gulp-cssnano');
var gcmq = require('gulp-group-css-media-queries');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var onError = function (err) {
    console.log('An error occurred:', gutil.colors.magenta(err.message));
    gutil.beep();
    this.emit('end');
};

gulp.task('frontcss', function() {
    return gulp.src('./src/sass/frontend/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        // .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gcmq())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('./netbase/css'))
        .pipe(cssnano({zindex:false}))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./netbase/css'));
        // .pipe(rtlcss())
        // .pipe(rename({ basename: 'rtl' }))
        // .pipe(gulp.dest('../'))
        // .pipe(cssnano({zindex:false}))
        // .pipe(rename({ basename: 'rtl', suffix: '.min' }))
        // .pipe(gulp.dest('../'));
});

// gulp.task('woocss', function() {
//     return gulp.src('./src/sass/woocommerce/*.scss')
//         .pipe(plumber({ errorHandler: onError }))
//         // .pipe(sourcemaps.init())
//         .pipe(sass())
//         .pipe(autoprefixer())
//         .pipe(gcmq())
//         // .pipe(sourcemaps.write())
//         .pipe(gulp.dest('./netbase/css'))
//         .pipe(cssnano({zindex:false}))
//         .pipe(rename({ suffix: '.min' }))
//         .pipe(gulp.dest('./netbase/css'));
//     // .pipe(rtlcss())
//     // .pipe(rename({ basename: 'rtl' }))
//     // .pipe(gulp.dest('../'))
//     // .pipe(cssnano({zindex:false}))
//     // .pipe(rename({ basename: 'rtl', suffix: '.min' }))
//     // .pipe(gulp.dest('../'));
// });

gulp.task('admincss', function() {
    return gulp.src('./src/sass/admin/**/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        // .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gcmq())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('./netbase/css/admin'))
        .pipe(cssnano({zindex:false}))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./netbase/css/admin'));
    // .pipe(rtlcss())
    // .pipe(rename({ basename: 'rtl' }))
    // .pipe(gulp.dest('../'))
    // .pipe(cssnano({zindex:false}))
    // .pipe(rename({ basename: 'rtl', suffix: '.min' }))
    // .pipe(gulp.dest('../'));
});

gulp.task('admin', function() {
    return gulp.src('./src/sass/admin/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gcmq())
        .pipe(gulp.dest('./netbase/css/admin'));
    // .pipe(cssnano({zindex:false}))
    // .pipe(rename({ suffix: '.min' }))
    // .pipe(gulp.dest('../'))
    // .pipe(rtlcss())
    // .pipe(rename({ basename: 'rtl' }))
    // .pipe(gulp.dest('../'))
    // .pipe(cssnano({zindex:false}))
    // .pipe(rename({ basename: 'rtl', suffix: '.min' }))
    // .pipe(gulp.dest('../'));
});

gulp.task('frontjs', function() {
    return gulp.src(['./src/js/frontend/*.js', './src/js/frontend/main/main.js'])
        // .pipe(jshint())
        // .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./netbase/js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./netbase/js'))
});

gulp.task('customizejs', function() {
    return gulp.src(['./src/js/admin/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./netbase/js/admin'))
});

gulp.task('watch', function() {
    // browserSync.init({
    //     files: ['../**/*.php'],
    //     proxy: 'http://localhost/core-wp/wp-admin/customize.php?return=%2Fcore-wp%2Fwp-admin%2F&changeset_uuid=70e6fbd5-c56f-4fdf-b1d7-95120898c71a',
    // });
    gulp.watch('src/sass/**/*.scss', ['frontcss', 'admincss']);
    gulp.watch('src/js/**/*.js', ['frontjs', 'customizejs']);
});
//
// gulp.task('watchcss', function() {
//     // browserSync.init({
//     //     files: ['../**/*.php'],
//     //     proxy: 'http://localhost/jewelry-tf',
//     // });
//     // gulp.watch('./src/sass/**/*.scss', ['frontcss']);
//     gulp.watch('./src/js/**/*.js', ['frontjs']);
// });

gulp.task('default', ['front', 'admin']);
